#[POST]user registration
curl -H "Content-Type: application/json" -d "{\"name\":\"Test001\",\"password\":\"0000\"}" -X POST -v https://api.testing.intray.cs-syd.eu/register

#[POST]user login
curl -H "Content-Type: application/json" -d "{\"username\":\"Test001\",\"password\":\"0000\"}" -X POST -v https://api.testing.intray.cs-syd.eu/login
: << 'END_COMMENT'
Returns a JWT token in the Set-Cookie header
Looks like this:

< Set-Cookie: JWT-Cookie=eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiYXV0aENvb2tpZVVzZXJVVUlEIjoiMDNkMWM2ZWUtNDBhNy00M2E4LWJhMDEtODc4NjUwZjI0NjI4IiwiYXV0aENvb2tpZVBlcm1pc3Npb25zIjpbIlBlcm1pdEFkZCIsIlBlcm1pdFNob3ciLCJQZXJtaXRTaXplIiwiUGVybWl0RGVsZXRlIiwiUGVybWl0R2V0SXRlbSIsIlBlcm1pdEdldEl0ZW1zIiwiUGVybWl0R2V0SXRlbVVVSURzIiwiUGVybWl0U3luYyIsIlBlcm1pdERlbGV0ZUFjY291bnQiLCJQZXJtaXRHZXRBY2NvdW50SW5mbyIsIlBlcm1pdFBvc3RBZGRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXlzIiwiUGVybWl0RGVsZXRlQWNjZXNzS2V5IiwiUGVybWl0R2V0UGVybWlzc2lvbnMiXX19.WHiVARjhYv13_UUew2Ls7JFno1j21pQmkgW4H_PFdh1PQB4yalV4srMBoiDQuJGm-fbASwbYEXNZT3UT9JUH2A; Path=/; HttpOnly; Secure
< Set-Cookie: XSRF-TOKEN=dzhIkhEUHjywbqfRW5dybAqyJvhgBOUXx+MwdRI7cus=; Path=/; Secure

END_COMMENT

#[GET]user information
curl -H "Authorization: Bearer <jwt>" -X GET -v https://api.testing.intray.cs-syd.eu/account
: << 'END_COMMENT'
e.g:
curl -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiYXV0aENvb2tpZVVzZXJVVUlEIjoiMDNkMWM2ZWUtNDBhNy00M2E4LWJhMDEtODc4NjUwZjI0NjI4IiwiYXV0aENvb2tpZVBlcm1pc3Npb25zIjpbIlBlcm1pdEFkZCIsIlBlcm1pdFNob3ciLCJQZXJtaXRTaXplIiwiUGVybWl0RGVsZXRlIiwiUGVybWl0R2V0SXRlbSIsIlBlcm1pdEdldEl0ZW1zIiwiUGVybWl0R2V0SXRlbVVVSURzIiwiUGVybWl0U3luYyIsIlBlcm1pdERlbGV0ZUFjY291bnQiLCJQZXJtaXRHZXRBY2NvdW50SW5mbyIsIlBlcm1pdFBvc3RBZGRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXlzIiwiUGVybWl0RGVsZXRlQWNjZXNzS2V5IiwiUGVybWl0R2V0UGVybWlzc2lvbnMiXX19.WHiVARjhYv13_UUew2Ls7JFno1j21pQmkgW4H_PFdh1PQB4yalV4srMBoiDQuJGm-fbASwbYEXNZT3UT9JUH2A" -X GET -v https://api.testing.intray.cs-syd.eu/account
END_COMMENT

#[POST]add an item
curl -H "Authorization: Bearer <jwt>" -H "Content-Type: application/json" -d "{\"data\": \"SGVsbG8gV29ybGQh\",\"type\": \"text\"}" -X POST -v https://api.testing.intray.cs-syd.eu/intray/item
: << END_COMMENT
where 'data' is base64 encoded

server will respond with UUID of added item

e.g:
curl -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiYXV0aENvb2tpZVVzZXJVVUlEIjoiMDNkMWM2ZWUtNDBhNy00M2E4LWJhMDEtODc4NjUwZjI0NjI4IiwiYXV0aENvb2tpZVBlcm1pc3Npb25zIjpbIlBlcm1pdEFkZCIsIlBlcm1pdFNob3ciLCJQZXJtaXRTaXplIiwiUGVybWl0RGVsZXRlIiwiUGVybWl0R2V0SXRlbSIsIlBlcm1pdEdldEl0ZW1zIiwiUGVybWl0R2V0SXRlbVVVSURzIiwiUGVybWl0U3luYyIsIlBlcm1pdERlbGV0ZUFjY291bnQiLCJQZXJtaXRHZXRBY2NvdW50SW5mbyIsIlBlcm1pdFBvc3RBZGRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXlzIiwiUGVybWl0RGVsZXRlQWNjZXNzS2V5IiwiUGVybWl0R2V0UGVybWlzc2lvbnMiXX19.WHiVARjhYv13_UUew2Ls7JFno1j21pQmkgW4H_PFdh1PQB4yalV4srMBoiDQuJGm-fbASwbYEXNZT3UT9JUH2A" -H "Content-Type: application/json" -d "{\"data\": \"SGVsbG8gV29ybGQh\",\"type\": \"text\"}" -X POST -v https://api.testing.intray.cs-syd.eu/intray/item
END_COMMENT


#[DELETE]delete an item
curl -H "Authorization: Bearer <jwt>" -H "Content-Type: application/json" -d "{\"username\":\"Test001\",\"password\":\"0000\"}" -X DELETE -v https://api.testing.intray.cs-syd.eu/intray/item

#[POST]synchronisation
curl -H "Authorization: Bearer <jwt>" -H "Content-Type: application/json" -d "{\"undeleted\":[<...>],\"unsynced\":[<...>],\"synced\":[<...>]}" -X POST -v https://api.testing.intray.cs-syd.eu/intray/sync
: << 'END_COMMENT'
curl -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiYXV0aENvb2tpZVVzZXJVVUlEIjoiMDNkMWM2ZWUtNDBhNy00M2E4LWJhMDEtODc4NjUwZjI0NjI4IiwiYXV0aENvb2tpZVBlcm1pc3Npb25zIjpbIlBlcm1pdEFkZCIsIlBlcm1pdFNob3ciLCJQZXJtaXRTaXplIiwiUGVybWl0RGVsZXRlIiwiUGVybWl0R2V0SXRlbSIsIlBlcm1pdEdldEl0ZW1zIiwiUGVybWl0R2V0SXRlbVVVSURzIiwiUGVybWl0U3luYyIsIlBlcm1pdERlbGV0ZUFjY291bnQiLCJQZXJtaXRHZXRBY2NvdW50SW5mbyIsIlBlcm1pdFBvc3RBZGRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXkiLCJQZXJtaXRHZXRBY2Nlc3NLZXlzIiwiUGVybWl0RGVsZXRlQWNjZXNzS2V5IiwiUGVybWl0R2V0UGVybWlzc2lvbnMiXX19.WHiVARjhYv13_UUew2Ls7JFno1j21pQmkgW4H_PFdh1PQB4yalV4srMBoiDQuJGm-fbASwbYEXNZT3UT9JUH2A" -H "Content-Type: application/json" -d "{\"undeleted\":[],\"unsynced\":[],\"synced\":[]}" -X POST -v https://api.testing.intray.cs-syd.eu/intray/sync
END_COMMENT
