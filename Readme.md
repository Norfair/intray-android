# Intray-Android

A mobile client for [Intray API](https://www.intray.eu).

**For the time being still under development and therefore connected to https://api.testing.intray.eu/ , _not_ to the main intray server.**

A user must create an account and log in prior to first use.

After login, he can add/process local queue even in the absence of internet connection. This works because the app caches the JWT token received upon login on the local device.

The app tries to sync with the server

- when the user starts the app
- when the user adds an item
- when the user deletes an item
- when the user selects the "processing" tab



## Backlog

- add widget (priority)
- "make it look pretty" / use intray brand identity (priority)
- make the app rotatable (not a priority)
- add background service periodically syncing local changes to server (not a priority)
    - frequency: "every 10min or so"
    - intention: pull changes from server s.t. the user can later process them, even in the absence of internet connection (i.e. without being able to sync _then_)
    - unclear if required/desirable at all


## Release steps

From: https://developer.android.com/studio/publish/app-signing

Build > Generate Signed Bundle / APK
Then you can find it in app/release.
