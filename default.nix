{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  buildGradle = pkgs.callPackage ./gradle-env.nix {};

in buildGradle {
  envSpec = ./gradle-env.json;

  src = ./.;

  # gradleFlags = [ "tasks" ];
  gradleFlags = [
    "build"
    "assemble"
    "-x" "lint" # It fails for nix reasons, just don't run it
  ];

  installPhase = ''
    mkdir -p $out
    cp -r app/build/outputs/apk/release/app-release-unsigned.apk $out/intray.apk
  '';
}
