package eu.cs_syd.intray.user.model

import eu.cs_syd.intray.common.data.persistence.isEncodable

/**
 * encapsulates user data
 */
interface IUser : isEncodable<IUser> {
    val name: String
    val serverName: String
    val creds: ICredentials

    val isLoggedIn: Boolean

    /**Prototype pattern
     * @return a clone of this [IUser] that has its [creds] set to [newCreds]*/
    fun withNewCredentials(newCreds: ICredentials): IUser
}