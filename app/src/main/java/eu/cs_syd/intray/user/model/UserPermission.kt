package eu.cs_syd.intray.user.model

sealed class UserPermission : IUserPermission {
    object PermitAdd : UserPermission()
    object PermitShow : UserPermission()
}