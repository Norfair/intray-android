package eu.cs_syd.intray.user.service

import eu.cs_syd.intray.user.model.UserLifecycleEvent

object UserEvents {
    interface UserLifecylceListener {
        fun onUserLifecycleChange(event: UserLifecycleEvent)
    }

    private var lastLifecycleEvent: UserLifecycleEvent? = null
    private val lifecycleSubscribers = mutableListOf<UserLifecylceListener>()

    fun subscribe(listener: UserLifecylceListener): UserLifecycleEvent? {
        synchronized(lifecycleSubscribers) {
            lifecycleSubscribers.add(listener)
            return lastLifecycleEvent
        }
    }

    fun removeListener(listener: UserLifecylceListener) {
        synchronized(lifecycleSubscribers) {
            lifecycleSubscribers.remove(listener)
        }
    }

    fun publish(event: UserLifecycleEvent) {
        lastLifecycleEvent = event
        notifySubscribers(event)
    }

    private fun notifySubscribers(event: UserLifecycleEvent) {
        synchronized(lifecycleSubscribers) {
            lifecycleSubscribers.forEach { it.onUserLifecycleChange(event) }
        }
    }


}