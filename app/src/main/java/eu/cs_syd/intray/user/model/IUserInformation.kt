package eu.cs_syd.intray.user.model

import java.util.Date

interface IUserInformation {
    val name: String
    val uuid: String
    val isAdmin: Boolean

    val created: Date
    val lastLogin: Date?

    val persistableString: String

    fun addPermission(perm: IUserPermission)
    fun removePermission(perm: IUserPermission)
    fun getPermissions(): Set<IUserPermission>
}