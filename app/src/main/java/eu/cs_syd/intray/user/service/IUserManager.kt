package eu.cs_syd.intray.user.service

import eu.cs_syd.intray.common.util.Callback
import eu.cs_syd.intray.user.model.IUser
import eu.cs_syd.intray.user.model.IUserInformation
import eu.cs_syd.intray.user.model.User

/**
 * provides access to user-spcific data
 */
interface IUserManager {
    /**called when a new user logs in*/
    fun onUserLogin(user: User)

    /**called when a user logs out*/
    fun onUserLogout(user: User)

    /**@return the [User] that is currently operating on the app*/
    fun getActiveUser(): User

    /**Queries the system for the [IUserInformation] about the [user].
     * May use either cached information or ask the server for a response.
     * @param user the [IUser] to be looked up
     * @param callback a [Callback] invoked when the request has been processed
     * @param forceRefresh `true` indicates that the system should not use cached information, defaults to `false`
     * */
    fun getUserInfo(user: IUser, callback: Callback<IUserInformation>, forceRefresh: Boolean = false)
}