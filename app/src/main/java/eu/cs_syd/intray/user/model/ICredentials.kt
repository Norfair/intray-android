package eu.cs_syd.intray.user.model

import eu.cs_syd.intray.common.data.persistence.isEncodable

/**
 * Encapsulates all the information necessary to prove to the server that a user is logged in.
 */
interface ICredentials : isEncodable<ICredentials> {
    val jwtToken: String
}