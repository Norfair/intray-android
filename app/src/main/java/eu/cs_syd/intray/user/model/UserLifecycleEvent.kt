package eu.cs_syd.intray.user.model

interface UserLifecycleEvent
class UserLoggedIn(val user: User) : UserLifecycleEvent
class UserLoggedOut(val user: User) : UserLifecycleEvent
