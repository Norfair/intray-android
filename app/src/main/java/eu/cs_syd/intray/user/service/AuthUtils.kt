package eu.cs_syd.intray.user.service

import eu.cs_syd.intray.network.HeaderNames.AUTHORIZATION
import eu.cs_syd.intray.network.HeaderNames.SET_COOKIE
import eu.cs_syd.intray.user.model.Credentials
import eu.cs_syd.intray.user.model.IUser
import java.util.Objects

object AuthUtils {
    class Header(name: String, val contents: String) {
        val name = name.toLowerCase()

        override fun toString(): String = "HEADER($name:$contents)"
        override fun hashCode(): Int = Objects.hash(name, contents)
        override fun equals(other: Any?): Boolean = when (other) {
            is Header -> name == other.name && contents == other.contents
            else -> false
        }
    }

    fun authorizationHeaderFor(user: IUser): Header {
        if (user.creds is Credentials.Invalid) throw IllegalArgumentException("user credentials are invalid")
        return Header(AUTHORIZATION, "Bearer ${user.creds.jwtToken}")
    }

    fun getJWTfromHeaders(headers: List<Header>): String? {
        headers.filter { it.name == SET_COOKIE }.forEach {
            for (kv in it.contents.split(";")) {
                val value = kv.trim()
                if (value.startsWith("JWT-Cookie=")) return value.replaceFirst("JWT-Cookie=", "")
            }
        }
        return null
    }
}