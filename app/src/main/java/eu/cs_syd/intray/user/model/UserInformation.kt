package eu.cs_syd.intray.user.model

import com.google.gson.JsonNull
import com.google.gson.JsonParser
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.config.DataConfig
import java.util.Date

/** Encapsulates user information, as returned by the server. */
class UserInformation(
    val user: IUser,
    override val uuid: String,
    override val isAdmin: Boolean,
    override val created: Date,
    override val lastLogin: Date?
) : IUserInformation {
    companion object {
        private const val KEY_UUID = "stringUUID"
        private const val KEY_IS_ADMIN = "booleanIsAdmin"
        private const val KEY_CREATED = "dateCreated"
        private const val KEY_LAST_LOGIN = "date?LastLogin"

        fun fromPersistableString(user: IUser, s: String): UserInformation {
            val jo = JsonParser().parse(s).asJsonObject
            val lastLogin = jo[KEY_LAST_LOGIN]

            return UserInformation(
                user,
                jo[KEY_UUID].asString,
                jo[KEY_IS_ADMIN].asBoolean,
                DataConfig.dateFormat.parse(jo[KEY_CREATED].asString),
                when (lastLogin) {
                    is JsonNull -> null
                    else -> DataConfig.dateFormat.parse(lastLogin.asString)
                }
            )
        }
    }

    override val name = user.name

    override val persistableString: String by lazy {
        json {
            KEY_UUID to uuid
            KEY_IS_ADMIN to isAdmin
            KEY_CREATED to DataConfig.dateFormat.format(created)
            KEY_LAST_LOGIN to lastLogin?.let { DataConfig.dateFormat.format(it) }
        }.toString()
    }
    private val permissions = mutableSetOf<IUserPermission>()
    override fun addPermission(perm: IUserPermission) {
        permissions.add(perm)
    }

    override fun removePermission(perm: IUserPermission) {
        permissions.remove(perm)
    }

    override fun getPermissions(): Set<IUserPermission> {
        return permissions.toSet()
    }
}