package eu.cs_syd.intray.user.model

import com.google.gson.JsonParser
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.data.persistence.IllegalEncoding
import eu.cs_syd.intray.common.data.persistence.isEncodable
import java.util.Objects

data class User(
    override val name: String,
    override val serverName: String,
    override val creds: ICredentials
) : IUser, isEncodable<IUser> {
    companion object {
        private const val ENCODE_KEY_TYPE = "type"
        private const val ENCODE_KEY_NAME = "name"
        private const val ENCODE_KEY_SERVER = "server"
        private const val ENCODE_KEY_CREDENTIALS = "creds"

        fun fromEncoding(encodedString: String): User {
            val encodedValues = JsonParser().parse(encodedString).asJsonObject

            val type = encodedValues.get(ENCODE_KEY_TYPE).asString
            return when (type) {
                User::class.java.name -> {
                    val name = encodedValues.get(ENCODE_KEY_NAME).asString
                    val server = encodedValues.get(ENCODE_KEY_SERVER).asString
                    val creds = Credentials.fromEncoding(encodedValues.get(ENCODE_KEY_CREDENTIALS).asString)

                    User(name, server, creds)
                }

                else -> throw IllegalEncoding("cannot parse to User: '$type', expected '${User::class.java.name}'")
            }

        }
    }

    override val isLoggedIn by lazy { creds is Credentials.Valid }

    override val stringEncoding: String by lazy {
        /*note: it is important to get the type OUTSIDE of the json{} block
        * within the json{} block, `this` refers to the `JsonHelper`*/
        val thisType = this::class.java.name
        val credsEncoding = creds.stringEncoding

        json {
            ENCODE_KEY_TYPE to thisType
            ENCODE_KEY_NAME to name
            ENCODE_KEY_SERVER to serverName
            ENCODE_KEY_CREDENTIALS to credsEncoding
        }.toString()
    }

    override fun withNewCredentials(newCreds: ICredentials): IUser {
        return User(name, serverName, newCreds)
    }

    override fun equals(other: Any?): Boolean {
        return if (other !is User) false
        else name == other.name && serverName == other.serverName && creds == other.creds
    }

    override fun hashCode(): Int {
        return Objects.hash(name, serverName, creds)
    }
}