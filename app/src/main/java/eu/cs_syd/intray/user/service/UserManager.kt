package eu.cs_syd.intray.user.service

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import com.android.volley.Response
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import eu.cs_syd.intray.account.service.AccountGetRequest
import eu.cs_syd.intray.account.service.IAccountGetResponse
import eu.cs_syd.intray.app.service.IntentManager.doLogin
import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.common.service.responses.errors.causes.Because
import eu.cs_syd.intray.common.util.Callback
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.network.volley.ResponseListeners
import eu.cs_syd.intray.network.volley.VolleyRequestDispatcher
import eu.cs_syd.intray.user.model.Credentials
import eu.cs_syd.intray.user.model.IUser
import eu.cs_syd.intray.user.model.IUserInformation
import eu.cs_syd.intray.user.model.User
import eu.cs_syd.intray.user.model.UserInformation
import eu.cs_syd.intray.user.model.UserLoggedIn
import eu.cs_syd.intray.user.model.UserLoggedOut
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class UserManager(application: Application) : IUserManager, AndroidViewModel(application) {

    companion object {
        private const val TAG = "[USER_MANAGER]"
        const val DEFAULT_USER_NAME = "\$OFFLINE"
        const val DEFAULT_SERVER_NAME = "\$NONE"

        const val PERSIST_STORAGE_NAME = "userData"

        //keys used to store to shared preferences
        private const val PERSIST_KEY_ACTIVE_USERS = "activeUsers"

        //keys used to store in [userData] map
        private const val DATA_KEY_USER_INFO = "info"
        private const val DATA_KEY_USER_THOUGHTS = "thoughts"

        private fun userPersistKey(user: IUser, suffix: String) = "${user.name}@${user.serverName}\$$suffix"
        fun PERSIST_KEY_THOUGHTS(user: IUser) = userPersistKey(user, "thoughts")
        private fun PERSIST_KEY_INFO(user: IUser) = userPersistKey(user, "info")
    }

    private val defaultUser: User = User(DEFAULT_USER_NAME, DEFAULT_SERVER_NAME, Credentials.Invalid)
    private var activeUser: User = defaultUser

    private val applicationContext: Context
        get() = this.getApplication<Application>().applicationContext

    /** maps from users to a map containing a (key:String,value:Any) pair
     * overrides [get] to add a new map for the [User] being looked up if none exists*/
    private val userData: MutableMap<User, MutableMap<String, Any>> = object : HashMap<User, MutableMap<String, Any>>() {
        override fun get(key: User): MutableMap<String, Any>? {
            val former = super.get(key)
            val current = former ?: mutableMapOf()
            if (current !== former) this.put(key, current)
            return current
        }
    }

    init {
        restoreActiveUsers()
        onUserLogin(getActiveUser())
    }

    override fun onCleared() {
        persistActiveUsersData()
    }

    override fun getActiveUser(): User {
        var currentUser = activeUser

        if (currentUser === defaultUser) {
            //try restoring persisted active user
            val decodedUsers = restoreActiveUsers()

            if (!decodedUsers.isEmpty()) {
                currentUser = decodedUsers.first()
                activeUser = currentUser
            }
        }

        return currentUser
    }

    /**sets [getActiveUser] to [user].
     *also makes sure there's a map for the [user] in [userData]
     * and restores the user's stored thoughts*/
    override fun onUserLogin(user: User) {
        activeUser = user

        //create a map in [userData]
        userData[user] ?: throw IllegalStateException("unexpected: no user data")

        if (user != defaultUser) UserEvents.publish(UserLoggedIn(user))
    }

    /** removes the [user] from persistent storage
     *  also removes the data in [userData] associated with that user
     *  and sets the [getActiveUser] to a the default, "offline" user*/
    override fun onUserLogout(user: User) {
        UserEvents.publish(UserLoggedOut(user))
        activeUser = defaultUser
        deletePersistedUser(user)
        userData.remove(user)
    }

    /**
     * @returns the active user
     * if no active user has been set, tries to restore it from shared preferences
     */

    /**saves the active users and the data associated wit them to persistent storage
     * for later restoration with [restoreActiveUsers]*/
    private fun persistActiveUsersData() {
        persistActiveUsers()
    }

    /**if a user is logged in, he will be stored to the shared preferences for later retrieval
     * if a user is logged in, there will be a [userData] entry with him as key*/
    private fun persistActiveUsers() {
        val persistableUsers = JsonArray()
        userData.keys.forEach {
            if (it !== defaultUser) persistableUsers.add(it.stringEncoding)
        }

        //store to persistent storage
        val prefs = applicationContext.getSharedPreferences(PERSIST_STORAGE_NAME, Context.MODE_PRIVATE)
        doAsync {
            uiThread {
                prefs.edit().putString(PERSIST_KEY_ACTIVE_USERS, persistableUsers.toString()).apply()
            }
        }
    }

    /**if a user logs out, he must be removed from shared preferences s.t. he's no longer retrievable
     * this will also delete the temporary record of the user's thoughts, so if these are to be retrievable,
     * they need be backed-up by calling [persistThoughts], before calling this method.*/
    private fun deletePersistedUser(user: IUser) {
        userData.remove(user) //remove the user from the list of active users
        if (user !== defaultUser) persistActiveUsers()  //recreate the persisted record
    }

    /**retrieves and restores active users formerly stored by [persistActiveUsersData]
     * data is restored on a need-to-have basis when it's requested
     * @return a set containing the persisted active user or an empty set if there is no persisted user or it could not be restored*/
    private fun restoreActiveUsers(): Set<User> {
        val prefs = applicationContext.getSharedPreferences(PERSIST_STORAGE_NAME, Context.MODE_PRIVATE)

        val encodedUsersString = prefs.getString(PERSIST_KEY_ACTIVE_USERS, null)

        //if nothing has been persisted, there's nothing to restore
        encodedUsersString ?: return emptySet()

        //otherwise parse the users
        val encodedUsers = JsonParser().parse(encodedUsersString).asJsonArray
        val decodedUsers: List<User> = try {
            encodedUsers.map { User.fromEncoding(it.asString) }
        } catch (e: Exception) {
            emptyList()
        }
        decodedUsers.forEach { userData[it] } //create an entry in [userData] with the user as key, if none exists

        return decodedUsers.toSet()
    }

    /**tries to restore the [user]'s information from shared preferences
     * and puts it into [userData]
     * @return the restored [IUserInformation] if restoration is possible, null otherwise*/
    private fun restoreInfo(user: IUser): IUserInformation? {
        val prefs = applicationContext.getSharedPreferences(PERSIST_STORAGE_NAME, Context.MODE_PRIVATE)
        val encodedThoughts: String? = prefs.getString(PERSIST_KEY_INFO(user), null)

        return encodedThoughts?.let {
            val info = UserInformation.fromPersistableString(user, it)
            userData[user]!![DATA_KEY_USER_INFO] = info
            info
        }
    }

    /**First looks the [user]'s information up in [userData].
     * If there's none stored there, tries to restore data from persistent storage.
     * If that too fails, fetches the info from the server.
     * @param forceRefresh if true, info is fetched from server, no matter what*/
    override fun getUserInfo(user: IUser, callback: Callback<IUserInformation>, forceRefresh: Boolean) {
        if (user === defaultUser) callback.onError(IllegalStateException("User is not logged in"))
        if (forceRefresh) fetchUserInfoFromServer(user, callback)
        else {
            val info = userData[user]!![DATA_KEY_USER_INFO] ?: restoreInfo(user)
            info?.let {
                callback.onResponse(it as IUserInformation)
            } ?: fetchUserInfoFromServer(user, callback)
        }
    }

    /**tries to fetch the user info from the server and if successful, caches it in the [userData] map,
     * using the [DATA_KEY_USER_INFO] key*/
    private fun fetchUserInfoFromServer(user: IUser, callback: Callback<IUserInformation>) {
        val req = AccountGetRequest(user)
        val listeners = ResponseListeners(
            Response.Listener {
                when (it) {
                    is IAccountGetResponse -> callback.onResponse(it.info)
                    is IServerError -> handleServerError(it, callback)
                    else -> throw IllegalArgumentException("unexpected response type `${it::class.java.name}`")
                }
            },
            Response.ErrorListener { callback.onError(it) }
        )
        val dispatcher = WebConfig.getRequestDispatcher() as VolleyRequestDispatcher
        val isAccepted = dispatcher.makeRequest(req, listeners)

        if (!isAccepted) toastAccountGetConnectivityIssues()
    }

    private fun handleServerError(error: IServerError, callback: Callback<*>? = null) {
        val reason = error.cause

        when (reason) {
            is Because.ServerRejectsLogin -> {
                doAsync { uiThread { applicationContext.longToast("sorry, your session timed out. please log in again.") } }
                doLogin(applicationContext)
            }
            else -> callback?.let { it.onError(reason) }
        }
    }

    private fun toastAccountGetConnectivityIssues() {
        doAsync { uiThread { applicationContext.toast("your device appears to be offline, we cannot get updated values") } }
    }
}