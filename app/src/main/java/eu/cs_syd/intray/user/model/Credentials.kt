package eu.cs_syd.intray.user.model

import com.google.gson.JsonParser
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.data.persistence.IllegalEncoding
import java.util.Objects

/**
 * Credentials are either Valid or Invalid.
 * A user with Invalid credentials will have to log in before he can sync.
 */
sealed class Credentials(override val jwtToken: String) : ICredentials {
    companion object {
        const val INVALID_JWT = "~~INVALID~~"

        private const val ENCODE_KEY_TYPE = "type"
        private const val ENCODE_KEY_JWT = "jwtToken"

        fun fromEncoding(encodedString: String): ICredentials {
            val encodedValues = JsonParser().parse(encodedString).asJsonObject


            val type = encodedValues.get(ENCODE_KEY_TYPE).asString
            return when (type) {
                Invalid::class.java.name -> Invalid
                Valid::class.java.name -> {
                    val jwtToken = encodedValues.get(ENCODE_KEY_JWT).asString
                    Valid(jwtToken)
                }
                else -> throw IllegalEncoding("cannot parse to Credentials: '$type'")
            }
        }

    }

    //TODO: we should probably confirm on construction that the passed jwt token adheres to expected format

    class Valid(jwtToken: String) : Credentials(jwtToken)
    object Invalid : Credentials(INVALID_JWT)

    override fun equals(other: Any?): Boolean {
        if (other !is Credentials) return false
        else if (other is Invalid) return this === other
        else return this.jwtToken == other.jwtToken
    }

    override fun hashCode(): Int {
        return Objects.hash(jwtToken)
    }

    override val stringEncoding: String by lazy {
        require(!jwtToken.contains("\"")) { "unexpected jwt: contains illegal character '\"'" }

        /*note: it is important to get the type OUTSIDE of the json{} block
        * within the json{} block, `this` refers to the `JsonHelper`*/
        val thisType = this::class.java.name

        json {
            ENCODE_KEY_TYPE to thisType
            ENCODE_KEY_JWT to jwtToken
        }.toString()
    }
}