package eu.cs_syd.intray.registration.service

import eu.cs_syd.intray.common.service.responses.IValidServerResponse

interface IRegistrationResponse : IValidServerResponse {
}