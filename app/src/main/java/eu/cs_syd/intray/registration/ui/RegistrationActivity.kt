package eu.cs_syd.intray.registration.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import eu.cs_syd.intray.R
import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.network.volley.ResponseListeners
import eu.cs_syd.intray.network.volley.VolleyRequestDispatcher
import eu.cs_syd.intray.registration.service.IRegistrationResponse
import eu.cs_syd.intray.registration.service.RegistrationRequest
import kotlinx.android.synthetic.main.activity_register.backButton
import kotlinx.android.synthetic.main.activity_register.passwordConfirmEditText
import kotlinx.android.synthetic.main.activity_register.passwordEditText
import kotlinx.android.synthetic.main.activity_register.registerButton
import kotlinx.android.synthetic.main.activity_register.usernameEditText
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class RegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        restoreSavedStrings()

        registerButton.setOnClickListener(this::onRegisterButtonPressed)
        backButton.setOnClickListener(this::onBackButtonPressed)
    }

    /**if the user entered a username and/or a password on the login screen,
     * displays them here as well*/
    private fun restoreSavedStrings() {
        intent.let {
            val un = it.getStringExtra("username")
            val pw = it.getStringExtra("password")
            usernameEditText.setText(un)
            passwordEditText.setText(pw)
        }
        if (usernameEditText.text.isEmpty()) usernameEditText.requestFocus()
        else if (passwordEditText.text.isEmpty()) passwordEditText.requestFocus()
        else passwordConfirmEditText.requestFocus()
    }

    /**checks user input and either invokes [makeRegistrationRequest] or displays an error message*/
    private fun onRegisterButtonPressed(view: View) {
        val password = passwordEditText.text.toString()
        val passwordConfirmation = passwordConfirmEditText.text.toString()

        if (password == passwordConfirmation) {
            val username = usernameEditText.text.toString()
            makeRegistrationRequest(username, password)
        } else doAsync { uiThread { toast("confirmation does not match password") } }
    }

    private fun onBackButtonPressed(view: View) {
        finish()
    }


    @Volatile
    private var isRegistrationRequestActive = false

    /**Does nothing if [isRegistrationRequestActive] is set.
     * Otherwise sets [isRegistrationRequestActive] and dispatches a registration request.
     * The response will unset [isRegistrationRequestActive]*/
    private fun makeRegistrationRequest(username: String, password: String) {
        val req = RegistrationRequest(username, password)
        val params = ResponseListeners(
            onResponse = Response.Listener {
                when (it) {
                    is IRegistrationResponse -> {
                        toastRegistrationSuccess();finish()
                    }
                    is IServerError -> toastRegistrationFailure(it.cause)
                    else -> throw IllegalArgumentException("unexpected response type `${it::class.java.name}`")
                }

            },
            onError = Response.ErrorListener { toastError(it) }
        )

        val dispatcher = WebConfig.getRequestDispatcher() as VolleyRequestDispatcher
        val isAccepted = dispatcher.makeRequest(req, params)
        if (!isAccepted) {
            isRegistrationRequestActive = false
            toastConnectivityIssues()
        }
    }

    private fun toastConnectivityIssues() {
        doAsync { uiThread { toast("please connect your device to the internet") } }
    }

    private fun toastError(cause: Throwable) {
        doAsync { uiThread { longToast("error: $cause") } }
        println(cause.stackTrace)
    }

    private fun toastRegistrationSuccess() {
        doAsync { uiThread { toast("thank you for registering") } }
    }

    private fun toastRegistrationFailure(cause: Throwable) {
        doAsync { uiThread { toast(cause.message ?: "sorry, we could not register you") } }
    }
}