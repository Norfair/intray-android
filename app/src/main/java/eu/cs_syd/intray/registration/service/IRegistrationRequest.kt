package eu.cs_syd.intray.registration.service

import eu.cs_syd.intray.common.service.requests.IServerRequest

interface IRegistrationRequest : IServerRequest {
    val username: String
    val password: String
}