package eu.cs_syd.intray.registration.service

import eu.cs_syd.intray.common.service.requests.IServerRequestVisitor

data class RegistrationRequest(override val username: String, override val password: String) : IRegistrationRequest {
    override fun <ParamType, ReturnType> accept(visitor: IServerRequestVisitor<ParamType, ReturnType>, params: ParamType): ReturnType {
        return visitor.visit(this, params)
    }
}