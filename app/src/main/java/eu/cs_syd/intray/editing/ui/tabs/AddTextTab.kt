package eu.cs_syd.intray.editing.ui.tabs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import eu.cs_syd.intray.R
import eu.cs_syd.intray.common.model.thoughts.ContentType
import eu.cs_syd.intray.common.model.thoughts.IThought
import eu.cs_syd.intray.common.model.thoughts.Thought
import eu.cs_syd.intray.common.util.Reply
import kotlinx.android.synthetic.main.fragment_add_component_text.textContent

class AddTextTab : AbstractAddTab() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_add_component_text, container, false)
    }

    /**sets the focus to the [textContent] and shows the soft keyboard if there is no hardware keyboard present*/
    override fun updateKeyboardStatus() {
        textContent.requestFocus()
        if (!existsHardwareKeyboard) {
            val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(activity?.currentFocus, 0)
        }

    }

    override fun createThought(): Reply<IThought> {
        val content = textContent.text
        return if (content.isEmpty()) Reply.Fail(IllegalStateException("no text"))
        else {
            val ret = Reply.OK<IThought>(Thought.Unsynced(ContentType.Text, data = content.toString().toByteArray()))
            content.clear()
            ret
        }
    }
}