package eu.cs_syd.intray.editing.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import eu.cs_syd.intray.R
import eu.cs_syd.intray.app.appViewModels
import eu.cs_syd.intray.app.ui.viewmodels.ThoughtsCollection
import eu.cs_syd.intray.common.model.thoughts.IThought
import eu.cs_syd.intray.common.util.Reply
import eu.cs_syd.intray.editing.ui.tabs.AddTextTab
import kotlinx.android.synthetic.main.fragment_add.view.btn_add
import kotlinx.android.synthetic.main.fragment_add.view.tab_container
import kotlinx.android.synthetic.main.fragment_add.view.tab_navigation
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread

class AddFragment : Fragment() {
    companion object {
        private const val TAB_TEXT = AddFragmentStatePagerAdapter.TEXT
    }

    private val thoughts: ThoughtsCollection by appViewModels()

    private lateinit var fragmentView: View
    private lateinit var tabNavigation: TabLayout
    private lateinit var tabContainer: ViewPager
    private lateinit var submitButton: FloatingActionButton
    private lateinit var fragmentAdapter: AddFragmentStatePagerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        fragmentView = inflater.inflate(R.layout.fragment_add, container, false)

        tabNavigation = fragmentView.tab_navigation
        tabContainer = fragmentView.tab_container
        submitButton = fragmentView.btn_add

        tabNavigation.setupWithViewPager(tabContainer)
        setupViewPager(tabContainer)

        submitButton.setOnClickListener(this::onSubmitButtonPressed)

        return fragmentView
    }

    /**creates a thought from the user input then requests a sync*/
    private fun onSubmitButtonPressed(view: View) {
        when (tabContainer.currentItem) {
            TAB_TEXT -> {
                val textTab = fragmentAdapter.fragment(TAB_TEXT) as AddTextTab?
                createTextThought(textTab ?: throw IllegalStateException("no AddTextTab available"))
                requestSync()
            }
        }
    }

    private fun setupViewPager(pager: ViewPager) {
        fragmentAdapter = AddFragmentStatePagerAdapter(childFragmentManager)
        pager.adapter = fragmentAdapter

        //listener that hides or shows the keyboard, depending on the tab selected
        pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {}

            /**does nothing, just implements the interface*/
            override fun onPageScrollStateChanged(state: Int) {
                updateKeyboardStatus()
            }

            /**does nothing, just implements the interface*/
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        })
    }

    private fun requestSync() {
        thoughts.requestSync()
    }

    private fun storeThought(thought: IThought) {
        thoughts.addThought(thought)
    }

    private fun createTextThought(tab: AddTextTab) {
        val attempt = tab.createThought()
        when (attempt) {
            is Reply.OK -> storeThought(attempt.result)
            is Reply.Fail -> when (attempt.cause) {
                is IllegalStateException -> doAsync { uiThread { activity?.toast("you cannot create empty thoughts") } }
                else -> throw attempt.cause
            }
        }
    }

    /**Decides whether or not to show the soft keyboard*/
    fun updateKeyboardStatus() {
        when (tabContainer.currentItem) {
            TAB_TEXT -> {
                val textTab = fragmentAdapter.fragment(TAB_TEXT) as AddTextTab?
                textTab?.updateKeyboardStatus()
            }
            else -> {
            }
        }
    }

}