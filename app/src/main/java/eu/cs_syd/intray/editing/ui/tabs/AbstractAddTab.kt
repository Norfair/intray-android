package eu.cs_syd.intray.editing.ui.tabs

import android.content.res.Configuration
import androidx.fragment.app.Fragment
import eu.cs_syd.intray.common.model.thoughts.IThought
import eu.cs_syd.intray.common.util.Reply

abstract class AbstractAddTab : Fragment() {
    /**Takes the information input on the tab and tries to convert it into an [IThought]*/
    abstract fun createThought(): Reply<IThought>

    /**@return true if there is a hardware keyboard that the user can use, false otherwise*/
    protected val existsHardwareKeyboard: Boolean
        get() = resources.configuration.keyboard != Configuration.KEYBOARD_NOKEYS

    /**Shows or hides the soft keyboard when switching to the tab.*/
    abstract fun updateKeyboardStatus()
}