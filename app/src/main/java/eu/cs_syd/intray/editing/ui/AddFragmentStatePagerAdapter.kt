package eu.cs_syd.intray.editing.ui

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import eu.cs_syd.intray.editing.ui.tabs.AddTextTab

class AddFragmentStatePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    companion object {
        private val NOF_VIEWS_AVAILABLE = 1

        const val TEXT = 0
    }

    override fun getCount(): Int {
        return NOF_VIEWS_AVAILABLE
    }


    private val fragmentTitle = arrayOf("TEXT")
    private val fragments = arrayOf<Fragment?>(null)
    fun fragment(position: Int): Fragment? {
        synchronized(fragments) {
            return fragments[position]
        }
    }

    private fun setFragment(position: Int, f: Fragment?) {
        synchronized(fragments) {
            fragments[position] = f
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            TEXT -> AddTextTab()
            else -> throw IllegalArgumentException("unexpected fragments position: $position")
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentTitle[position]
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        setFragment(position, fragment)
        return fragment
    }
}