package eu.cs_syd.intray.editing.data

interface IBase64EncoderDecoder {
    /**encodes a [ByteArray] into a Base64-encoding*/
    fun toBase64(data: ByteArray): String

    /**decodes a Base64-encoding string to a [ByteArray]*/
    fun fromBase64(encodedData: String): ByteArray
}