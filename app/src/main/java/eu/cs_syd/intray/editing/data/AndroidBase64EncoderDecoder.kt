package eu.cs_syd.intray.editing.data

import android.util.Base64

/**encapsulates access to [Base64] s.t. we don't need to pollute domain object with android-specific classes*/
class AndroidBase64EncoderDecoder : IBase64EncoderDecoder {

    override fun toBase64(data: ByteArray): String {
        val encoding = String(Base64.encode(data, Base64.NO_WRAP), Charsets.UTF_8)
        return encoding
    }

    override fun fromBase64(encodedData: String): ByteArray {
        return Base64.decode(encodedData, Base64.DEFAULT)
    }
}