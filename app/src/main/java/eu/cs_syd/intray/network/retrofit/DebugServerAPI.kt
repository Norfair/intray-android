package eu.cs_syd.intray.network.retrofit

import com.google.gson.JsonObject
import eu.cs_syd.intray.network.BodyContentType.JSON
import eu.cs_syd.intray.network.HeaderNames.AUTHORIZATION
import eu.cs_syd.intray.network.HeaderNames.CONTENT_TYPE
import retrofit2.Call
import retrofit2.http.*

/**Provides lower-level access to webservice in order to circumvent domain objects
 * Should be used for testing only.*/
interface DebugServerAPI {
    @Headers("$CONTENT_TYPE:$JSON")
    @POST("/login")
    fun loginJWT(@Body loginInfo: JsonObject): Call<JsonObject>

    @Headers("$CONTENT_TYPE:$JSON")
    @POST("/intray/sync")
    fun sync(@Header(AUTHORIZATION) authorization: String, @Body syncRequest: JsonObject): Call<JsonObject>

    @Headers("$CONTENT_TYPE:$JSON")
    @GET("/intray/item/{uuid}")
    fun getItem(@Header(AUTHORIZATION) authorization: String, @Path("uuid") itemUUID: String): Call<JsonObject>
}