package eu.cs_syd.intray.network.volley.requests

import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.common.service.responses.IValidServerResponse
import eu.cs_syd.intray.common.service.responses.errors.causes.Because
import eu.cs_syd.intray.common.service.responses.errors.causes.RequestDenied
import eu.cs_syd.intray.network.BodyContentType


/**
 * @param method one of [com.android.volley.Request.Method]
 * @param serverUrl the url the request should be sent to
 * @param listener handles [IServerResponse]s, may receive an [IValidServerResponse] or an [IServerError]
 * @param errorListener handles unexpected errors
 */
abstract class AbstractVolleyRequest(
    method: Int,
    serverUrl: String,
    protected val listener: Response.Listener<IServerResponse>,
    errorListener: Response.ErrorListener
) : Request<IServerResponse>(method, serverUrl, errorListener) {
    companion object {
        const val TAG_REQUEST = "[VOLLEY REQUEST]"
        const val TAG_ERROR = "[VOLLEY RESPONSE ERROR]"
    }

    override fun getBodyContentType(): String {
        return BodyContentType.JSON
    }

    override fun deliverResponse(response: IServerResponse) = listener.onResponse(response)
    override fun deliverError(error: VolleyError?) {
        val response = error?.networkResponse
        val statusCode = response?.statusCode ?: -1
        when (statusCode) {
            401 -> {//unauthorised
                deliverResponse(RequestDenied(Because.ServerRejectsLogin("login invalid or timed out")))
            }
            else -> {
                if (response == null) {
                    Log.d(TAG_ERROR, error?.localizedMessage ?: "ERROR IS NULL")
                    deliverResponse(RequestDenied(Because.Unknown))
                    return
                }
                val headers = response.headers
                val data = response.data?.let { String(it, Charsets.UTF_8).replace(",", "\n") }
                Log.d(TAG_ERROR, "status code: $statusCode")
                Log.d(TAG_ERROR, "headers:\n$headers")
                Log.d(TAG_ERROR, "data (as String):\n$data")
                throw IllegalStateException("""
                    NETWORK ERROR
                    status code: $statusCode
                    headers:
                        $headers
                    data (as String):
                       $data
                """.trimIndent())
            }
        }
    }

    override fun toString(): String {
        return """
            |$TAG_REQUEST ${this.javaClass.simpleName}
            |   body-content-type: ${this.bodyContentType}
            |   headers: ${this.headers}
            |   body: ${String(this.body)}
        """.trimMargin()
    }
}