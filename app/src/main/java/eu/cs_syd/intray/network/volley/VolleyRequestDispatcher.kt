package eu.cs_syd.intray.network.volley

import android.content.Context
import android.util.Log
import com.android.volley.VolleyLog
import com.android.volley.toolbox.Volley
import eu.cs_syd.intray.account.service.IAccountGetRequest
import eu.cs_syd.intray.common.service.requests.IRequestDispatcher
import eu.cs_syd.intray.common.service.requests.IServerRequest
import eu.cs_syd.intray.common.service.requests.IServerRequestVisitor
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.login.service.ILoginRequest
import eu.cs_syd.intray.network.volley.requests.VolleyAccountGetRequest
import eu.cs_syd.intray.network.volley.requests.VolleyLoginRequest
import eu.cs_syd.intray.network.volley.requests.VolleyRegisterRequest
import eu.cs_syd.intray.network.volley.requests.VolleySyncRequest
import eu.cs_syd.intray.registration.service.IRegistrationRequest
import eu.cs_syd.intray.synchronisation.service.ISyncRequest
import org.jetbrains.anko.doAsync
import java.net.InetAddress
import java.net.UnknownHostException

typealias ResponseListeners = VolleyResponseListeners<IServerResponse>

class VolleyRequestDispatcher(
    context: Context
) : IServerRequestVisitor<ResponseListeners, Unit>, IRequestDispatcher<ResponseListeners> {
    val TAG = "[VOLLEY REQ DISPATCHER]"

    private val queue = Volley.newRequestQueue(context)

    init {
        VolleyLog.DEBUG = true
    }

    val isNetworkAvailable: Boolean
        get() {
            var result = false
            doAsync {
                try {
                    result = !InetAddress.getByName("google.com").equals("")
                } catch (e: UnknownHostException) {
                    result = false
                }
            }.get()
            return result
        }

    override fun makeRequest(req: IServerRequest, params: ResponseListeners): Boolean {
        return if (isNetworkAvailable) {
            req.accept(this, params)
            true
        } else false
    }


    override fun visit(req: IRegistrationRequest, params: ResponseListeners) {
        val serverURL = WebConfig.getServerURL()
        val request = VolleyRegisterRequest(serverURL, req, params.onResponse, params.onError)
        Log.d(TAG, "\n" + request.toString())
        queue.add(request)
    }

    override fun visit(req: ILoginRequest, params: ResponseListeners) {
        val serverURL = WebConfig.getServerURL()
        val request = VolleyLoginRequest(serverURL, req, params.onResponse, params.onError)
        Log.d(TAG, "\n" + request.toString())
        queue.add(request)
    }

    override fun visit(req: IAccountGetRequest, params: ResponseListeners) {
        val serverURL = WebConfig.getServerURL()
        val request = VolleyAccountGetRequest(serverURL, req, params.onResponse, params.onError)
        Log.d(TAG, "\n" + request.toString())
        queue.add(request)
    }

    override fun visit(req: ISyncRequest, params: ResponseListeners) {
        val serverURL = WebConfig.getServerURL()
        val request = VolleySyncRequest(serverURL, req, params.onResponse, params.onError)
        Log.d(TAG, "\n" + request.toString())
        queue.add(request)
    }
}