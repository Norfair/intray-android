package eu.cs_syd.intray.network.volley.requests

import android.util.Log
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.VolleyError
import com.google.gson.JsonNull
import com.google.gson.JsonParser
import eu.cs_syd.intray.account.service.AccountGetResponse
import eu.cs_syd.intray.account.service.IAccountGetRequest
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.config.DataConfig
import eu.cs_syd.intray.user.model.UserInformation
import eu.cs_syd.intray.user.service.AuthUtils

class VolleyAccountGetRequest(
    serverURL: String,
    private val accountGetRequest: IAccountGetRequest,
    listener: Response.Listener<IServerResponse>,
    errorListener: Response.ErrorListener
) : AbstractVolleyRequest(Method.GET, "$serverURL/account", listener, errorListener) {

    override fun getHeaders(): MutableMap<String, String> {
        val auth = AuthUtils.authorizationHeaderFor(accountGetRequest.user)
        val headers = mutableMapOf(
            auth.name to auth.contents
        )
        return headers
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<IServerResponse> {
        Log.d("REQ IN", response.toString())
        return response?.let {
            if (it.statusCode != 200) return Response.error<IServerResponse>(VolleyError("illegal status code: ${it.statusCode}"))

            val jsonString = String(response.data, Charsets.UTF_8)
            val encodedValues = JsonParser().parse(jsonString).asJsonObject

            val name = encodedValues.get("username").asString
            val expectedName = accountGetRequest.user.name
            if (name != expectedName) return Response.error<IServerResponse>(VolleyError("wanted information for '$expectedName' but got information on '$name'"))
            else {
                val uuid = encodedValues.get("uuid").asString
                val isAdmin = encodedValues.get("admin").asBoolean

                val df = DataConfig.dateFormat
                val created = df.parse(encodedValues.get("created").asString)
                val lastLogin = encodedValues.get("last-login")?.let {
                    if (it is JsonNull) null
                    else df.parse(it.asString)
                }

                val info = UserInformation(accountGetRequest.user, uuid, isAdmin, created, lastLogin)

                Response.success(AccountGetResponse(info) as IServerResponse, null)
            }
        } ?: Response.error<IServerResponse>(VolleyError(IllegalArgumentException("null reply")))
    }
}