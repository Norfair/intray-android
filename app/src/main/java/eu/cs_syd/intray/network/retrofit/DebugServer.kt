package eu.cs_syd.intray.network.retrofit

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.model.thoughts.Thought
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.ThoughtSet
import eu.cs_syd.intray.synchronisation.service.SyncRequest
import eu.cs_syd.intray.user.model.Credentials
import eu.cs_syd.intray.user.model.User
import eu.cs_syd.intray.user.service.AuthUtils
import eu.cs_syd.intray.user.service.AuthUtils.getJWTfromHeaders
import kotlinx.coroutines.*
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset.defaultCharset
import kotlin.coroutines.CoroutineContext

/**An auxiliary retrofit controller to facilitate communication with the server during testing.*/
object DebugServer : CoroutineScope {
    private const val TAG = "[TEST::SERVER]"
    const val URL = "https://api.testing.intray.eu"
    const val USERNAME = "UnitTest"
    const val PASSWORD = "0000"

    private val job = Job()
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    private val retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(DebugServerAPI::class.java)

    /**uses [USERNAME] and [PASSWORD] to get a valid jwt from the test server at [URL]*/
    val JWT: String by lazy {
        val loginInfo = json {
            "username" to USERNAME
            "password" to PASSWORD
        }

        val resp = retrofit.loginJWT(loginInfo).execute()

        if (!resp.isSuccessful) throw IllegalStateException("request failed with ${resp.code()}: ${resp.body()}")
        val headers = utilHeaders(resp)

        getJWTfromHeaders(headers) ?: throw IllegalStateException("could not extract user jwt")
    }

    val testUser by lazy {
        User(USERNAME, URL, Credentials.Valid(JWT))
    }

    val authorization by lazy {
        "Bearer $JWT"
    }

    /**Transforms [resp]'s headers to a list of [AuthUtils.Header]s*/
    private fun utilHeaders(resp: Response<*>): List<AuthUtils.Header> = resp.headers().toMultimap()
        .flatMap { kv -> kv.value.map { AuthUtils.Header(kv.key, it) } }

    fun clearItems() {
        val currentItemResponse = runBlocking { trySync(SyncRequest(testUser, ThoughtSet())).await() }
        if (!currentItemResponse.isSuccessful) throw java.lang.IllegalStateException("Sync Request failed unexpectedly")

        val currentItemIds = currentItemResponse.body()!!.get("server-added").asJsonObject.keySet()

        val deletingItemRequest = SyncRequest(testUser, ThoughtSet(
            *currentItemIds.map { Thought.Undeleted(it) }.toTypedArray()
        ))

        val deletingItemResponse = runBlocking { trySync(deletingItemRequest).await() }
        if (!deletingItemResponse.isSuccessful) throw java.lang.IllegalStateException("Sync Request failed unexpectedly")
    }

    /**Sends the [syncRequest] to the server.
     * @return a [Deferred] holding the server [Response]*/
    fun trySync(syncRequest: JsonObject): Deferred<Response<JsonObject>> {
        val header = authorization
        logRequest("SYNC", header, syncRequest)
        return async {
            val response = retrofit.sync(header, syncRequest).execute()
            logResponse("SYNC", response)
            response
        }
    }

    fun trySync(syncRequest: SyncRequest): Deferred<Response<JsonObject>> {
        return trySync(Gson().fromJson(syncRequest.jsonRepresentation, JsonObject::class.java))
    }

    fun retrieveItemById(itemUUID: String): Synced? {
        logRequest("GET-ITEM", authorization, JsonObject())
        val response = retrofit.getItem(authorization, itemUUID).execute()
        logResponse("GET-ITEM", response)

        return response.body()?.let { Synced(it) }
    }

    private fun logRequest(requestType: String, header: String, body: JsonObject) {
        Log.d(TAG, """REQUEST $requestType ==>
            HEADER: $header
            ${GsonBuilder().setPrettyPrinting().create().toJson(body)}
            """.trimIndent())
    }

    //@formatter:off
    private fun logResponse(requestType: String, response: Response<JsonObject>) {
        Log.d(TAG, """<== $requestType RESPONSE
            SUCCESS:    ${if (response.isSuccessful) "YES" else "NO"}
            CODE:       ${response.code()}
            ${GsonBuilder().setPrettyPrinting().create().toJson(
                if (response.isSuccessful) response.body()
                else response.errorBody()?.source()?.readString(defaultCharset())
            )}
        """.trimIndent())
    }
    //@formatter:on
}