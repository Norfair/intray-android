package eu.cs_syd.intray.network.volley.requests

import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.VolleyError
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.common.service.responses.errors.causes.Because
import eu.cs_syd.intray.common.service.responses.errors.causes.RequestDenied
import eu.cs_syd.intray.network.BodyContentType
import eu.cs_syd.intray.registration.service.IRegistrationRequest
import eu.cs_syd.intray.registration.service.RegistrationResponse

class VolleyRegisterRequest(
    serverURL: String,
    private val registrationRequest: IRegistrationRequest,
    listener: Response.Listener<IServerResponse>,
    errorListener: Response.ErrorListener
) : AbstractVolleyRequest(Method.POST, "$serverURL/register", listener, errorListener) {

    override fun getBodyContentType(): String {
        return BodyContentType.JSON
    }

    override fun getBody(): ByteArray {
        return json {
            "name" to registrationRequest.username
            "password" to registrationRequest.password
        }.toString().toByteArray()
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<IServerResponse> {
        return response?.let {
            Response.success(RegistrationResponse() as IServerResponse, null)
        } ?: Response.error(VolleyError(IllegalArgumentException("null reply")))
    }

    override fun deliverResponse(response: IServerResponse) = listener.onResponse(response)

    override fun deliverError(error: VolleyError?) {
        val response = error?.networkResponse
        val statusCode = response?.statusCode ?: -1
        when (statusCode) {
            409 -> {//conflict
                val msg = String(response!!.data, Charsets.UTF_8)
                deliverResponse(RequestDenied(Because.UserAlreadyExists(msg)) as IServerResponse)
            }
            else -> super.deliverError(error)
        }
    }
}