package eu.cs_syd.intray.network.volley.requests

import android.util.Log
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.VolleyError
import com.google.gson.JsonParser
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.network.BodyContentType
import eu.cs_syd.intray.synchronisation.service.ISyncRequest
import eu.cs_syd.intray.synchronisation.service.SyncResponse
import eu.cs_syd.intray.user.service.AuthUtils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class VolleySyncRequest(
    serverURL: String,
    private val syncRequest: ISyncRequest,
    listener: Response.Listener<IServerResponse>,
    errorListener: Response.ErrorListener
) : AbstractVolleyRequest(Method.POST, "$serverURL/intray/sync", listener, errorListener) {
    companion object {
        const val TAG = "[SYNC REQUEST]"
    }


    override fun getHeaders(): MutableMap<String, String> {
        val auth = AuthUtils.authorizationHeaderFor(syncRequest.user)
        val headers = mutableMapOf(
            auth.name to auth.contents
        )
        return headers
    }

    override fun getBodyContentType(): String {
        return BodyContentType.JSON
    }

    override fun getBody(): ByteArray {
        val body = syncRequest.jsonRepresentation
        doAsync {
            uiThread {
                println(TAG + body)
            }
        }

        return body.toByteArray()
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<IServerResponse> {
        return response?.let {
            val jsonString = String(response.data, Charsets.UTF_8)
            val encodedValues = JsonParser().parse(jsonString).asJsonObject

            val syncResponse = SyncResponse(syncRequest, encodedValues)
            Log.d("VOLLEY RESP IN", syncResponse.toString())
            return Response.success(syncResponse, null)
        } ?: Response.error<IServerResponse>(VolleyError("null response"))
    }
}