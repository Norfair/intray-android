package eu.cs_syd.intray.network.volley

import com.android.volley.Request
import com.android.volley.Response

/**
 * A set of listeners used by a [Request] to handle the result ofthe attempted
 * parsing of the [Response] it gets.
 *
 * [ResponseType] is the type Volley will attempt to parse the [Response] into.
 */
data class VolleyResponseListeners<ResponseType>(
    /**Called IFF the corresponding [Request] succeeds in parsing its [Response]*/
    val onResponse: Response.Listener<ResponseType>,

    /**Called if there was an error parsing the [Response]*/
    val onError: Response.ErrorListener
)