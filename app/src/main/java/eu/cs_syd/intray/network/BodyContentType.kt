package eu.cs_syd.intray.network

object BodyContentType {
    const val JSON = "application/json"
}