package eu.cs_syd.intray.network.volley.requests

import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.VolleyError
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.service.responses.IServerResponse
import eu.cs_syd.intray.login.service.ILoginRequest
import eu.cs_syd.intray.login.service.LoginResponse
import eu.cs_syd.intray.user.model.Credentials
import eu.cs_syd.intray.user.service.AuthUtils

class VolleyLoginRequest(
    private val serverURL: String,
    private val loginRequest: ILoginRequest,
    listener: Response.Listener<IServerResponse>,
    errorListener: Response.ErrorListener
) : AbstractVolleyRequest(Method.POST, "$serverURL/login", listener, errorListener) {

    override fun getBody(): ByteArray {
        return json {
            "username" to loginRequest.username
            "password" to loginRequest.password
        }.toString().toByteArray()
    }

    override fun parseNetworkResponse(response: NetworkResponse?): Response<IServerResponse> {
        return response?.let {
            val headers = response.allHeaders.map { AuthUtils.Header(it.name, it.value) }
            val jwtToken = AuthUtils.getJWTfromHeaders(headers)
                ?: return Response.error(VolleyError("no jwt provided"))
            val creds = Credentials.Valid(jwtToken)
            Response.success(LoginResponse(serverURL, creds) as IServerResponse, null)
        } ?: Response.error(VolleyError(IllegalArgumentException("null reply")))
    }
}