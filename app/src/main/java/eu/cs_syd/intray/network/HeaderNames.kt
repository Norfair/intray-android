package eu.cs_syd.intray.network

/**convention: all header names are lowercase*/
object HeaderNames {
    const val AUTHORIZATION = "authorization"
    const val SET_COOKIE = "set-cookie"
    const val CONTENT_TYPE = "content-type"
}