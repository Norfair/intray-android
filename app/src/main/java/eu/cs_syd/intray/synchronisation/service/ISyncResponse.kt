package eu.cs_syd.intray.synchronisation.service

import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import eu.cs_syd.intray.common.model.thoughts.Thought
import eu.cs_syd.intray.common.service.responses.IValidServerResponse

/**A server replies with three arrays
 * - new : thoughts on the server that the client did not know about when he made the request
 * - deleted : thoughts deleted from the server
 * - added : thoughts that this client created that have now been confirmed to have been synced
 *
 * This class collects them and merges them with the local thoughts.
 */
interface ISyncResponse : IValidServerResponse {
    /**thoughts that have been on the server that the local device does not know about*/
    val new: Set<IThoughtWithContent>

    /**UUIDs of thoughts deleted from the server*/
    val deleted: Set<String>

    /**thoughts that were created on the local device and that have now been confirmed by the
     * are acknowledged and can be elevated to [Thought.Synced]*/
    val acknowledged: Map<ULong, Thought.Unsynced.Ack>
}