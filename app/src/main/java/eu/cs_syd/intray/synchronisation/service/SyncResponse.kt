package eu.cs_syd.intray.synchronisation.service

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced

class SyncResponse(syncRequest: ISyncRequest, parsableResponse: JsonObject) : ISyncResponse {
    override lateinit var new: Set<IThoughtWithContent>
    override lateinit var deleted: Set<String>
    override lateinit var acknowledged: Map<ULong, Unsynced.Ack>

    init {
        val serverAdded = parsableResponse.get("server-added").asJsonObject
        val serverDeleted = parsableResponse.get("server-deleted").asJsonArray
        val clientAdded = parsableResponse.get("client-added").asJsonObject
        val clientDeleted = parsableResponse.get("client-deleted").asJsonArray

        val gson = Gson()
        val serverAddedType = object : TypeToken<Map<String, JsonObject>>() {}.type
        val encodedMapping: Map<String, JsonObject> = gson.fromJson(serverAdded, serverAddedType)

        new = encodedMapping
            .map { it.value.apply { addProperty("id", it.key) } }
            .map { Synced(it) }
            .toSet()

        deleted = serverDeleted
            .map { it.asString }.toSet()
            .union(clientDeleted.map { it.asString }.toSet())

        acknowledged = Unsynced.Ack.fromJsonMap(clientAdded)
    }

    override fun toString(): String {
        return """
            SyncResponse{
                newThoughts = ${new}
                deleted = $deleted
                added = $acknowledged
            }
        """.trimIndent()
    }
}