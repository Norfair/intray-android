package eu.cs_syd.intray.synchronisation.service

import eu.cs_syd.intray.common.service.requests.IServerRequest
import eu.cs_syd.intray.user.model.IUser

/**A server expects three arrays:
 * - undeleted : the UUIDs of thoughts deleted from this client whose deletion has not yet been confirmed by the server
 * - unsynced : thoughts added from this client that the server does not yet know about
 * - synced : the UUIDs of thoughts that this client knows have been synced to the server
 *
 * This class encapsulates the local thoughts and returns them in a server-understandable Json string.
 * */
interface ISyncRequest : IServerRequest {
    /**The user making the request.*/
    val user: IUser

    /**a server-understandable representation of the local thoughts*/
    val jsonRepresentation: String
}