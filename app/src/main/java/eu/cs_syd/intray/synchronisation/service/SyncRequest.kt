package eu.cs_syd.intray.synchronisation.service

import com.google.gson.JsonArray
import com.google.gson.JsonParser
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.model.thoughts.IThoughtSet
import eu.cs_syd.intray.common.model.thoughts.Thought
import eu.cs_syd.intray.common.service.requests.IServerRequestVisitor
import eu.cs_syd.intray.user.model.IUser

/**captures a snapshot of [thoughts] for synchronization with the server.*/
class SyncRequest(override val user: IUser, val thoughts: IThoughtSet) : ISyncRequest {
    override fun <ParamType, ReturnType> accept(visitor: IServerRequestVisitor<ParamType, ReturnType>, params: ParamType): ReturnType {
        return visitor.visit(this, params)
    }

    private var cachedJson: String? = null

    override val jsonRepresentation: String
        get() {

            cachedJson?.let { return it }

            val undeleted = JsonArray()
            val unsynced = mutableMapOf<ULong, Thought.Unsynced>()
            val synced = JsonArray()

            val deletedUUIDs = mutableSetOf<String>()

            var unsyncedId = 1
            thoughts.forEach {
                when (it) {
                    is Thought.Unsynced -> unsynced[unsyncedId++.toULong()] = it
                    is Thought.Synced -> synced.add(it.syncRepresentation)
                    is Thought.Undeleted -> {
                        undeleted.add(it.syncRepresentation)
                        deletedUUIDs.add(it.UUID)
                    }

                }
            }

            val jsonString = json {
                "deleted" to undeleted
                "added" to unsynced
                "synced" to synced
            }.toString()
            cachedJson = jsonString
            return jsonString
        }

    override fun toString(): String {
        val json = JsonParser().parse(this.jsonRepresentation).asJsonObject

        return """
            SyncRequest(user=$user)
            $json
        """.trimIndent()
    }
}