package eu.cs_syd.intray.config

import eu.cs_syd.intray.common.service.requests.IRequestDispatcher

object WebConfig : IWebConfig {
    private const val DEFAULT_SERVER_URL = "https://api.intray.eu"

    override fun getServerURL(): String {
        return DEFAULT_SERVER_URL
    }

    private var requestDispatcher: IRequestDispatcher<*>? = null

    override fun setServerRequestDispatcher(requestDispatcher: IRequestDispatcher<*>) {
        WebConfig.requestDispatcher = requestDispatcher
    }

    override fun getRequestDispatcher(): IRequestDispatcher<*> {
        return requestDispatcher
            ?: throw IllegalStateException("no request dispatcher has been set")
    }
}
