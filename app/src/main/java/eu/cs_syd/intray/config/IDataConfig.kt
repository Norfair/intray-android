package eu.cs_syd.intray.config

import java.text.SimpleDateFormat
import java.util.Locale

/** Provides global access to data features and properties */
interface IDataConfig {
    val locale: Locale
    val dateFormat: SimpleDateFormat
}