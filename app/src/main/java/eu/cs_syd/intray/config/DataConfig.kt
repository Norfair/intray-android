package eu.cs_syd.intray.config

import java.text.SimpleDateFormat
import java.util.Locale

object DataConfig : IDataConfig {

    private const val DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    override var locale: Locale = Locale.ENGLISH
        private set
    override var dateFormat: SimpleDateFormat = SimpleDateFormat(DATE_FORMAT_PATTERN, locale)
        private set
}