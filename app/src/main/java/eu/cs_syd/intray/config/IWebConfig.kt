package eu.cs_syd.intray.config

import eu.cs_syd.intray.common.service.requests.IRequestDispatcher

/** Provides global access to networking features and properties */
interface IWebConfig {
    fun getServerURL(): String
    fun setServerRequestDispatcher(requestDispatcher: IRequestDispatcher<*>)
    fun getRequestDispatcher(): IRequestDispatcher<*>
}