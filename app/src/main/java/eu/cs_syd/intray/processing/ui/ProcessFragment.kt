package eu.cs_syd.intray.processing.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import eu.cs_syd.intray.R
import eu.cs_syd.intray.account.ui.contentviews.TextContentView
import eu.cs_syd.intray.app.appViewModels
import eu.cs_syd.intray.app.ui.viewmodels.ThoughtsCollection
import eu.cs_syd.intray.common.model.thoughts.ContentType
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import kotlinx.android.synthetic.main.fragment_process.view.btn_delete
import kotlinx.android.synthetic.main.fragment_process.view.view_container
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ProcessFragment : Fragment() {
    companion object {
        private const val VIEW_EMPTY = ProcessFragmentStatePagerAdapter.EMPTY
        private const val VIEW_TEXT = ProcessFragmentStatePagerAdapter.TEXT
    }

    private val thoughts: ThoughtsCollection by appViewModels()
    private lateinit var fragmentView: View
    private lateinit var viewContainer: ViewPager
    private lateinit var pagerAdapter: ProcessFragmentStatePagerAdapter
    private lateinit var deleteButton: FloatingActionButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)

        fragmentView = inflater.inflate(R.layout.fragment_process, container, false)
        viewContainer = fragmentView.view_container
        deleteButton = fragmentView.btn_delete

        setupViewPager(viewContainer)

        thoughts.nextThought.observe(viewLifecycleOwner, Observer { displayNextThought(it) }) //TODO: figure out why this doesn't get executed as often as it should be
        deleteButton.setOnClickListener { dequeue() }

        return fragmentView
    }

    private fun setupViewPager(pager: ViewPager) {
        pagerAdapter = ProcessFragmentStatePagerAdapter(childFragmentManager)
        pager.adapter = pagerAdapter
    }

    private fun dequeue() {
        thoughts.removeNextThought()
    }

    private fun displayNextThought(thought: IThoughtWithContent?) {
        thought?.let {
            when (thought.type) {
                ContentType.Text -> displayTextThought(thought)
                else -> throw IllegalArgumentException("don't know how to handle content type ${thought.type}")
            }
        } ?: displayNoThought()
    }

    private fun displayNoThought() {
        viewContainer.setCurrentItem(VIEW_EMPTY, false)
    }

    private fun displayTextThought(thought: IThoughtWithContent) {
        viewContainer.setCurrentItem(VIEW_TEXT, false)

        //update the text view that eventually gets displayed:
        doAsync {
            var fragment: TextContentView?
            do {
                fragment = pagerAdapter.fragment(VIEW_TEXT) as TextContentView?
            } while (fragment == null)
            uiThread {
                fragment.displayThought(thought)
            }
        }
    }
}