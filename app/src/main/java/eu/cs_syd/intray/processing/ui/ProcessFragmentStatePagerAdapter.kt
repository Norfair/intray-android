package eu.cs_syd.intray.processing.ui

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import eu.cs_syd.intray.account.ui.contentviews.NoContentView
import eu.cs_syd.intray.account.ui.contentviews.TextContentView

class ProcessFragmentStatePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    companion object {
        private const val NOF_VIEWS_AVAILABLE = 2

        const val EMPTY = 0
        const val TEXT = 1
    }

    private val fragments = arrayOf<Fragment?>(null, null)
    fun fragment(position: Int): Fragment? {
        synchronized(fragments) {
            return fragments[position]
        }
    }

    private fun setFragment(position: Int, f: Fragment?) {
        synchronized(fragments) {
            fragments[position] = f
        }
    }

    override fun getCount(): Int {
        return NOF_VIEWS_AVAILABLE
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            EMPTY -> NoContentView()
            TEXT -> TextContentView()
            else -> throw IllegalArgumentException("unexpected fragments position: $position")
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        setFragment(position, fragment)
        return fragment
    }
}