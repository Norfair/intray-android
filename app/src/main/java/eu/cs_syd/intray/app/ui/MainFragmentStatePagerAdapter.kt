package eu.cs_syd.intray.app.ui

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import eu.cs_syd.intray.account.ui.AccountFragmentWithoutUserInfo
import eu.cs_syd.intray.editing.ui.AddFragment
import eu.cs_syd.intray.processing.ui.ProcessFragment

class MainFragmentStatePagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    companion object {
        private const val NOF_VIEWS_AVAILABLE = 3

        const val PROCESS = 0
        const val ADD = 1
        const val ACCOUNT = 2
    }

    private val fragments = arrayOf<Fragment?>(null, null, null)
    fun fragment(position: Int): Fragment? {
        synchronized(fragments) {
            return fragments[position]
        }
    }

    private fun setFragment(position: Int, f: Fragment?) {
        synchronized(fragments) {
            fragments[position] = f
        }
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            PROCESS -> ProcessFragment()
            ADD -> AddFragment()
            ACCOUNT -> AccountFragmentWithoutUserInfo()
            else -> throw IllegalArgumentException("unexpected fragment position: $position")
        }
    }

    override fun getCount(): Int {
        return NOF_VIEWS_AVAILABLE
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position) as Fragment
        setFragment(position, fragment)
        return fragment
    }
}