package eu.cs_syd.intray.app.service

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import eu.cs_syd.intray.login.ui.LoginActivity

/**provides global access point for intent dispatch*/
object IntentManager {
    /**switches to the [LoginActivity] to ask the user to sign in*/
    fun doLogin(context: Context) {
        val intent = Intent(context, LoginActivity::class.java)
        intent.flags = FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }
}