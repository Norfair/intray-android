package eu.cs_syd.intray.app

import android.app.Activity
import android.app.Application
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner

class App : Application(), ViewModelStoreOwner, ViewModelProvider.Factory {
    companion object {
        lateinit var instance: App
            private set
    }

    private val appViewModelStore: ViewModelStore by lazy { ViewModelStore() }
    private val viewModelFactory: ViewModelProvider.AndroidViewModelFactory by lazy { ViewModelProvider.AndroidViewModelFactory(this) }
    private val viewModelProvider: ViewModelProvider by lazy { ViewModelProvider(this, viewModelFactory) }

    override fun getViewModelStore(): ViewModelStore {
        return appViewModelStore
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModelProvider.get(modelClass)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    fun clearViewModelStore() {
        appViewModelStore.clear()
    }
}

class ApplicationScopedViewModel<VM : ViewModel>(private val modelClazz: Class<VM>) : Lazy<VM> {
    private var model: VM? = null

    override val value: VM
        get() {
            model = model ?: App.instance.create(modelClazz)
            return model!!
        }

    override fun isInitialized(): Boolean {
        return model != null
    }
}

@MainThread
inline fun <reified VM : ViewModel> Fragment.appViewModels(): Lazy<VM> {
    return ApplicationScopedViewModel(VM::class.java)
}

@MainThread
inline fun <reified VM : ViewModel> Activity.appViewModels(): Lazy<VM> {
    return ApplicationScopedViewModel(VM::class.java)
}
