package eu.cs_syd.intray.app.ui

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import eu.cs_syd.intray.R
import eu.cs_syd.intray.R.id.navigation
import eu.cs_syd.intray.R.id.navigation_account
import eu.cs_syd.intray.R.id.navigation_add
import eu.cs_syd.intray.R.id.navigation_process
import eu.cs_syd.intray.account.ui.AbstractAccountFragment
import eu.cs_syd.intray.account.ui.AccountFragmentWithUserInfo
import eu.cs_syd.intray.app.App
import eu.cs_syd.intray.app.appViewModels
import eu.cs_syd.intray.app.service.Encoder
import eu.cs_syd.intray.app.service.IntentManager
import eu.cs_syd.intray.app.ui.viewmodels.ThoughtsCollection
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.editing.data.AndroidBase64EncoderDecoder
import eu.cs_syd.intray.editing.ui.AddFragment
import eu.cs_syd.intray.network.volley.VolleyRequestDispatcher
import eu.cs_syd.intray.processing.ui.ProcessFragment
import eu.cs_syd.intray.user.model.IUser
import eu.cs_syd.intray.user.service.UserManager
import kotlinx.android.synthetic.main.activity_main.fragment_container
import kotlinx.android.synthetic.main.navigation_main_bottom.navigation
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread
import kotlin.math.max
import kotlin.system.measureNanoTime

class MainActivity : AppCompatActivity() {

    private val userManager: UserManager by appViewModels()
    private val thoughts: ThoughtsCollection by appViewModels()
    private lateinit var fragmentViewPager: ViewPager

    companion object {
        private const val FRAGMENT_ACCOUNT = MainFragmentStatePagerAdapter.ACCOUNT
        private const val FRAGMENT_PROCESS = MainFragmentStatePagerAdapter.PROCESS
        private const val FRAGMENT_ADD = MainFragmentStatePagerAdapter.ADD
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val elapsedNano = measureNanoTime {
            super.onCreate(savedInstanceState)

            setContentView(R.layout.activity_main)

            WebConfig.setServerRequestDispatcher(VolleyRequestDispatcher(applicationContext))
            Encoder.base64EncoderDecoder = AndroidBase64EncoderDecoder()
            fragmentViewPager = fragment_container
            setupViewPager(fragmentViewPager)

            //navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            navigation.setOnNavigationItemSelectedListener {
                when (it.itemId) {
                    navigation_process -> {
                        setFragment(FRAGMENT_PROCESS);true
                    }
                    navigation_add -> {
                        setFragment(FRAGMENT_ADD);true
                    }
                    navigation_account -> {
                        setFragment(FRAGMENT_ACCOUNT);true
                    }
                    else -> false
                }
            }

            val au = userManager.getActiveUser()
            if (!au.isLoggedIn) IntentManager.doLogin(this)
            else validateLogin(au)
        }

        reportNano("onCreate", elapsedNano)
    }

    override fun onStop() {
        App.instance.clearViewModelStore()
        super.onStop()
    }

    /**Sets the pager's adapter to a new[MainFragmentStatePagerAdapter]
     * Also adds a [ViewPager.OnPageChangeListener] to the [pager] that updates the [navigation] when a user
     * switches fragments by swiping rather than clicking the [navigation]'s menu items.
     * Further adds listeners that update the [ProcessFragment] and [AccountFragmentWithUserInfo] when the user switches to them.
     **/
    private fun setupViewPager(pager: ViewPager) {
        val adapter = MainFragmentStatePagerAdapter(supportFragmentManager)
        pager.adapter = adapter
        pager.currentItem = FRAGMENT_ADD

        //listener that updates the bottom navigation
        val bottomNavigationUpdater = object : ViewPager.OnPageChangeListener {
            private var lastSelected = -1
            /**updates the [navigation] s.t. the menu item corresponding to the selected page is checked*/
            override fun onPageSelected(position: Int) {
                val currSelection = max(lastSelected, 0)

                val menu = navigation.menu
                menu.getItem(currSelection).isChecked = false
                menu.getItem(position).isChecked = true
            }

            /**does nothing, just implements the interface*/
            override fun onPageScrollStateChanged(state: Int) {}

            /**does nothing, just implements the interface*/
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        }
        pager.addOnPageChangeListener(bottomNavigationUpdater)

        //listener that hides or shows the keyboard when switching to fragments, as appropriate
        val keyboardHiderShower = object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                when (position) {
                    FRAGMENT_PROCESS, FRAGMENT_ACCOUNT -> hideKeyboardIfVisible()
                    FRAGMENT_ADD -> {
                        val adapter = fragmentViewPager.adapter as MainFragmentStatePagerAdapter
                        val addfragment = adapter.fragment(FRAGMENT_ADD) as AddFragment?
                        addfragment?.updateKeyboardStatus()
                    }
                    else -> {
                    }
                }
            }

            /**does nothing, just implements the interface*/
            override fun onPageScrollStateChanged(state: Int) {}

            /**does nothing, just implements the interface*/
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        }
        pager.addOnPageChangeListener(keyboardHiderShower)

        //listener that updates the process view when you switch to it
        val processFragmentUpdater = object : ViewPager.OnPageChangeListener {
            /**calls [ProcessFragment]::displayNextThought() on the currently displayed [ProcessFragment]*/
            override fun onPageSelected(position: Int) {
                if (position == FRAGMENT_PROCESS) {
                    thoughts.requestSync()
                }
            }

            /**does nothing, just implements the interface*/
            override fun onPageScrollStateChanged(state: Int) {}

            /**does nothing, just implements the interface*/
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        }
        pager.addOnPageChangeListener(processFragmentUpdater)

        //listener that updates the account fragment when you switch to it
        val accountFragmentUpdater = object : ViewPager.OnPageChangeListener {

            /**calls [AccountFragmentWithUserInfo]::fetchUserData() on the currently displayed [AccountFragmentWithUserInfo]]*/
            override fun onPageSelected(position: Int) {
                if (position == FRAGMENT_ACCOUNT) {
                    doAsync {
                        var fragment: AbstractAccountFragment?
                        do {
                            fragment = adapter.fragment(FRAGMENT_ACCOUNT) as AbstractAccountFragment?
                        } while (fragment == null)
                        uiThread { fragment.doUpdate() }
                    }
                }
            }

            /**does nothing, just implements the interface*/
            override fun onPageScrollStateChanged(state: Int) {}

            /**does nothing, just implements the interface*/
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
        }
        pager.addOnPageChangeListener(accountFragmentUpdater)
    }

    /**Changes the fragment being displayed in the main activity.
     * [MainActivity]'s companion object holds constants and it is adviced to use these instead of
     * hardcoding the [fragment] value passed.*/
    fun setFragment(fragment: Int) {
        require(fragment >= 0) { "invalid fragment index, '$fragment'" }
        fragmentViewPager.currentItem = fragment
    }

    private fun hideKeyboardIfVisible() {
        val view = currentFocus
        view?.let {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(it.windowToken, 0)
        }
    }

    /**onResume: greet user*/
    override fun onResume() {
        val au = userManager.getActiveUser()
        if (au.isLoggedIn) doAsync { uiThread { toast("welcome back, ${au.name}") } }
        super.onResume()
    }

    /**Checks whether the credentials provided by [user] are still valid*/
    private fun validateLogin(user: IUser) {
        require(user.isLoggedIn)

        //TODO
    }

    private fun reportNano(label: String, elapsedNano: Long) {
        println("MainActivity::$label completed in $elapsedNano ns = ${elapsedNano / 1e6}ms")
    }
}
