package eu.cs_syd.intray.app.ui.viewmodels

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.core.util.Preconditions.checkState
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.volley.Response
import eu.cs_syd.intray.app.service.IntentManager.doLogin
import eu.cs_syd.intray.common.model.thoughts.IThought
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import eu.cs_syd.intray.common.model.thoughts.ThoughtSet
import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.common.service.responses.errors.causes.Because
import eu.cs_syd.intray.common.util.Callback
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.network.volley.ResponseListeners
import eu.cs_syd.intray.network.volley.VolleyRequestDispatcher
import eu.cs_syd.intray.synchronisation.service.ISyncResponse
import eu.cs_syd.intray.synchronisation.service.SyncRequest
import eu.cs_syd.intray.user.model.User
import eu.cs_syd.intray.user.model.UserLifecycleEvent
import eu.cs_syd.intray.user.model.UserLoggedIn
import eu.cs_syd.intray.user.model.UserLoggedOut
import eu.cs_syd.intray.user.service.UserEvents
import eu.cs_syd.intray.user.service.UserEvents.UserLifecylceListener
import eu.cs_syd.intray.user.service.UserManager.Companion.PERSIST_KEY_THOUGHTS
import eu.cs_syd.intray.user.service.UserManager.Companion.PERSIST_STORAGE_NAME
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread

class ThoughtsCollection : AndroidViewModel, UserLifecylceListener {
    companion object {
        const val TAG = "[THOUGHTS COLLECTION]"
    }

    private var user: User? = null
    private var thoughts = ThoughtSet()
    private var _nextThought: MutableLiveData<IThoughtWithContent?> = MutableLiveData(null)

    val nextThought: LiveData<IThoughtWithContent?>
        get() = _nextThought

    private val applicationContext: Context
        get() = this.getApplication<Application>().applicationContext

    constructor(application: Application) : super(application) {
        UserEvents.subscribe(this)?.let { onUserLifecycleChange(it) }
    }

    @Synchronized
    override fun onCleared() {
        UserEvents.removeListener(this)
        persistThoughts()
        user = null
    }

    @Synchronized
    override fun onUserLifecycleChange(event: UserLifecycleEvent) {
        when (event) {
            is UserLoggedIn -> {
                if (event.user != this.user) {
                    this.user = event.user
                    restoreThoughts()
                }
            }
            is UserLoggedOut -> onCleared()
            else -> throw IllegalArgumentException("Unknown user lifecycle event: ${event.javaClass.name}")
        }
    }

    @Synchronized
    fun removeNextThought() {
        checkState(user != null, "sanity check failed: no user set")
        thoughts.pop()
        updateNextThought()
    }

    @Synchronized
    fun addThought(thought: IThought) {
        checkState(user != null, "sanity check failed: no user set")
        thoughts.addThought(thought)
        updateNextThought()
    }

    @Synchronized
    private fun merge(syncResponse: ISyncResponse) {
        thoughts.mergeWith(syncResponse)
        updateNextThought()
    }

    private fun updateNextThought() {
        _nextThought.postValue(thoughts.peek())
    }

    @Volatile
    private var isSyncRunning = false

    /**schedules a synchronisation of the local collection with the server-side one */
    @Synchronized
    fun requestSync() {
        val user = user ?: return
        synchronized(this) {
            if (isSyncRunning) return
            isSyncRunning = true
        }

        Log.d(TAG, "thoughts before SYNC = $thoughts")
        val req = SyncRequest(user, thoughts)
        val listeners = ResponseListeners(
            Response.Listener {
                when (it) {
                    is ISyncResponse -> {
                        val thoughtsCollection = this
                        doAsync {
                            thoughtsCollection.merge(it)
                        }
                    }
                    is IServerError -> handleServerError(it)
                    else -> throw IllegalArgumentException("unexpected response type '${it::class.java.name}'")
                }
                isSyncRunning = false
                Log.d(TAG, "thoughts after SYNC = $thoughts")
            },
            Response.ErrorListener { throw it }
        )
        val dispatcher = WebConfig.getRequestDispatcher() as VolleyRequestDispatcher
        val isAccepted = dispatcher.makeRequest(req, listeners)
        if (!isAccepted) isSyncRunning = false
    }

    /**stores the current [user]'s thoughts to persistent memory for later retrieval*/
    private fun persistThoughts() {
        val user = user ?: return
        val prefs = applicationContext.getSharedPreferences(PERSIST_STORAGE_NAME, Context.MODE_PRIVATE)
        doAsync {
            uiThread {
                prefs.edit().putString(PERSIST_KEY_THOUGHTS(user), thoughts.persistableEncoding()).apply()
            }
        }
    }

    /**tries to restore a [ThoughtSet] for the current [user] from persistent memory*/
    @Synchronized
    private fun restoreThoughts() {
        val user = user ?: return
        val prefs = applicationContext.getSharedPreferences(PERSIST_STORAGE_NAME, Context.MODE_PRIVATE)
        val encodedThoughts: String? = prefs.getString(PERSIST_KEY_THOUGHTS(user), null)

        this.thoughts = encodedThoughts?.let { ThoughtSet.fromEncoding(encodedThoughts) }
            ?: ThoughtSet()
        updateNextThought()
    }

    private fun handleServerError(error: IServerError, callback: Callback<*>? = null) {
        val reason = error.cause

        when (reason) {
            is Because.ServerRejectsLogin -> {
                doAsync { uiThread { applicationContext.longToast("sorry, your session timed out. please log in again.") } }
                doLogin(applicationContext)
            }
            else -> callback?.let { it.onError(reason) }
        }
    }
}