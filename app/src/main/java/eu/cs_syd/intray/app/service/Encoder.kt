package eu.cs_syd.intray.app.service

import eu.cs_syd.intray.editing.data.IBase64EncoderDecoder

object Encoder {
    var base64EncoderDecoder: IBase64EncoderDecoder? = null

    fun toBase64(data: ByteArray) = base64EncoderDecoder?.toBase64(data)
        ?: throw IllegalStateException("no base64 encoder set")

    fun fromBase64(encodedData: String) = base64EncoderDecoder?.fromBase64(encodedData)
        ?: throw IllegalStateException("no base64 decoder set")
}