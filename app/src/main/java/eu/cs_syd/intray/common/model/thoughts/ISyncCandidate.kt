package eu.cs_syd.intray.common.model.thoughts


interface ISyncCandidate {
    /**abstracts the [ISyncCandidate] to the [String] representation needed
     * for a successful sync */
    val syncRepresentation: Any
}