package eu.cs_syd.intray.common.threading

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job


class BasicCoroutineScope(dispatcher: CoroutineDispatcher = Dispatchers.IO) : CoroutineScope {
    private val job = Job()
    override val coroutineContext = job + dispatcher

    fun close() = job.cancel()
}