package eu.cs_syd.intray.common.model.thoughts

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import eu.cs_syd.intray.app.service.Encoder
import eu.cs_syd.intray.common.data.json
import eu.cs_syd.intray.common.data.persistence.IllegalEncoding
import eu.cs_syd.intray.config.DataConfig
import java.util.*

sealed class Thought : IThought {
    companion object {
        fun fromPersistedString(encoding: String): IThought {

            with(JsonParser().parse(encoding).asJsonObject) {
                val type = get("type").asString
                return when (type) {
                    Unsynced::class.java.name -> unsynced(this)
                    Synced::class.java.name -> synced(this)
                    Undeleted::class.java.name -> undeleted(this)
                    else -> throw IllegalEncoding("unrecognised thought class '$type'")
                }
            }
        }

        private fun unsynced(encoding: JsonObject): Unsynced {
            with(encoding) {
                val desc = get("desc").asJsonObject
                val timestampCreated = DataConfig.dateFormat.parse(desc.get("created").asString)
                with(desc.get("contents").asJsonObject) {
                    val type = ContentType.parse(get("type").asString)
                    val dataEncoding = get("data").asString

                    return Unsynced(type = type, dataEncoding = dataEncoding, timestampCreated = timestampCreated)
                }
            }
        }

        private fun synced(encoding: JsonObject): Synced {
            return Synced(encoding.get("desc").asJsonObject)
        }

        private fun undeleted(encoding: JsonObject): Undeleted {
            return Undeleted(encoding.get("uuid").asString)
        }
    }

    abstract class ThoughtWithContent : IThoughtWithContent, Comparable<IThoughtWithContent>, Thought() {
        override fun equals(other: Any?): Boolean {
            return when {
                this === other -> true
                other is IThoughtWithContent -> timestampCreated == other.timestampCreated && data.contentEquals(other.data)
                else -> false
            }
        }

        override fun hashCode(): Int {
            return Objects.hash(timestampCreated, data)
        }

        /**Compares the ages of the thoughts.
         * @return 0 if the thoughts are equally old,
         * a value smaller 0 if this thought is older than the other thought,
         * a value greater 0 if this thought is younger than the other thought*/
        override fun compareTo(other: IThoughtWithContent): Int {
            return timestampCreated.compareTo(other.timestampCreated)
        }
    }

    /**An item the server is aware of.*/
    class Synced : ThoughtWithContent {
        val UUID: String
        override val type: IContentType
        override val data: ByteArray
        override val timestampCreated: Date

        override val syncRepresentation: String
            get() = UUID

        override val persistableEncoding: String

        /**[Synced] thoughts are only ever received from the server.
         * @param encodedSyncedThought a json object encapsulating all the information received from the server
         */
        constructor(encodedSyncedThought: JsonObject) : super() {
            with(encodedSyncedThought) {
                UUID = get("id").asString

                val df = DataConfig.dateFormat
                timestampCreated = df.parse(get("created").asString)

                val dataObj = get("contents").asJsonObject
                with(dataObj) {
                    type = ContentType.parse(get("type").asString)
                    data = Encoder.fromBase64(get("data").asString)
                }
            }

            persistableEncoding = json {
                "type" to Synced::class.java.name
                "desc" to encodedSyncedThought
            }.toString()
        }

        override fun toString(): String {
            return "Synced(UUID='$UUID', type=$type)"
        }
    }

    /**An item that has been created locally and was not yet synced with the server*/
    class Unsynced : ThoughtWithContent {
        data class Ack(val uuid: String) {
            companion object {
                fun fromJsonMap(json: JsonObject): Map<ULong, Ack> {
                    return json.entrySet().map { entry ->
                        val uuid = entry.value.asString
                        entry.key.toULong() to Ack(uuid)
                    }.toMap()
                }
            }
        }

        override lateinit var type: IContentType
        override lateinit var data: ByteArray
        override lateinit var timestampCreated: Date

        override val syncRepresentation: JsonObject by lazy {
            json {
                "contents" to json {
                    "data" to Encoder.toBase64(data)
                    "type" to type.toString()
                }
                "created" to DataConfig.dateFormat.format(timestampCreated)
            }
        }

        override val persistableEncoding: String by lazy {
            json {
                "type" to Unsynced::class.java.name
                "desc" to syncRepresentation
            }.toString()
        }

        /**@return a [Synced] version of this, constructed with the content of this [Unsynced] and
         * the additional information provided by the [Ack]*/
        fun elevate(ack: Ack): Synced {
            val jsonRepresentation = syncRepresentation.deepCopy()

            jsonRepresentation.apply {
                addProperty("id", ack.uuid)
            }

            return Synced(jsonRepresentation)
        }

        /**
         * either [dataEncoding] or [data] can be null.
         * if [data] is null, it will be generated by decoding [dataEncoding]
         * if both [dataEncoding] AND [data] are provided, no checks are performed to validate the dataEncoding
         *
         * Only [data] is stored within the object so it can be later displayed.
         * The [dataEncoding] is recreated on a need-to-have basis.
         *
         * @param type the [IContentType] corresponding to this thought
         * @param dataEncoding a String representation of the [data] that is understood by the server, if null: [data] will be encoded
         * @param data the decoded form of the [dataEncoding], if null: decoded from [dataEncoding]
         * @param timestampCreated the date at which this [Thought] was created if null, a new one will be generated
         * */
        constructor(
            type: IContentType,
            dataEncoding: String? = null,
            data: ByteArray? = null,
            timestampCreated: Date? = null
        ) : super() {
            require(dataEncoding != null || data != null) { "must provide either dataEncoding or data" }

            this.type = type
            this.data = data ?: Encoder.fromBase64(dataEncoding!!)
            val df = DataConfig.dateFormat
            this.timestampCreated = df.parse(timestampCreated?.let { df.format(it) }
                ?: df.format(Date()))
        }

        override fun toString(): String {
            return "Unsynced(type=$type)"
        }

    }

    /**an item that has been deleted locally but whose deletion has not yet been communicated to the server**/
    data class Undeleted(override val UUID: String) : Thought(), IDeletedThought {
        override val syncRepresentation = UUID

        override val persistableEncoding: String by lazy {
            json {
                "type" to Undeleted::class.java.name
                "uuid" to UUID
            }.toString()
        }

        override fun equals(other: Any?): Boolean {
            return if (other !is IDeletedThought) false else UUID == other.UUID
        }

        override fun hashCode(): Int {
            return Objects.hash(UUID)
        }

        override fun toString(): String {
            return "Undeleted(UUID='$UUID')"
        }

    }

}