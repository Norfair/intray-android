package eu.cs_syd.intray.common.model.thoughts

sealed class ContentType(val textualRepresentation: String) : IContentType {
    override fun toString(): String {
        return textualRepresentation
    }

    companion object {
        fun parse(textualRepresentation: String): ContentType {
            return when (textualRepresentation) {
                Text.toString() -> Text
                else -> throw IllegalArgumentException("unknown content type '$textualRepresentation'")
            }
        }
    }

    object Text : ContentType("text")
}