package eu.cs_syd.intray.common.data.persistence

interface isEncodable<T> {
    /**a String from which an instance can be recreated*/
    val stringEncoding: String
}