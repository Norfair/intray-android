package eu.cs_syd.intray.common.service.responses

/**responses when the server refuses the request*/
interface IServerError : IServerResponse {
    val cause: Throwable
}