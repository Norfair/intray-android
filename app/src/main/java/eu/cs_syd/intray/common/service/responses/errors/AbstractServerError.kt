package eu.cs_syd.intray.common.service.responses.errors

import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.common.service.responses.errors.causes.Because

abstract class AbstractServerError(
    override val cause: Because
) : IServerError