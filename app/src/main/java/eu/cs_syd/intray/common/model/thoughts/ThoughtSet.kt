package eu.cs_syd.intray.common.model.thoughts

import android.util.Log
import com.google.common.collect.Iterators
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Undeleted
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.synchronisation.service.ISyncResponse
import java.util.TreeSet

/**A [TreeSet]-based [IThought], returning the items in the order they have been created.
 * The access methods are synchronized to make this collection thread-safe.*/
class ThoughtSet() : IThoughtSet {
    companion object {
        const val TAG = "ThoughtSet"

        fun fromEncoding(encoding: String): ThoughtSet {
            val arr = JsonParser().parse(encoding).asJsonArray
            val ts = ThoughtSet()
            arr.forEach { ts.addThought(Thought.fromPersistedString(it.asString)) }
            return ts
        }

        private const val DEBUG_MERGE = false
    }

    constructor(vararg thoughts: IThought) : this() {
        addAll(*thoughts)
    }

    /**contains all not-deleted items*/
    private val thoughts = TreeSet<IThoughtWithContent>()

    /**contains all items that were deleted on the client but not yet synced*/
    private val undeletedThoughts = mutableSetOf<IDeletedThought>()

    /**Contains all items that were created on the client but not yet synced */
    private val unsyncedThoughts = mutableMapOf<ULong, Unsynced>()

    private val nextUnsyncedIndex: ULong
        get() = unsyncedThoughts.keys.max() ?: 0.toULong() + 1.toULong()

    override val size: Int
        get() {
            return synchronized(thoughts) { thoughts.size } + synchronized(undeletedThoughts) { undeletedThoughts.size }
        }

    override fun isEmpty(): Boolean {
        return synchronized(thoughts) { thoughts.isEmpty() } && synchronized(undeletedThoughts) { undeletedThoughts.isEmpty() }
    }

    override fun persistableEncoding(): String {
        val array = JsonArray()
        synchronized(thoughts) { thoughts.forEach { array.add(it.persistableEncoding) } }
        synchronized(undeletedThoughts) { undeletedThoughts.forEach { array.add(it.persistableEncoding) } }
        return array.toString()
    }

    override fun addThought(thought: IThought) {
        when (thought) {
            is Thought.ThoughtWithContent -> synchronized(thoughts) {
                thoughts.add(thought)
                if (thought is Unsynced) {
                    unsyncedThoughts[nextUnsyncedIndex] = thought
                }
            }
            is Undeleted -> synchronized(undeletedThoughts) { undeletedThoughts.add(thought) }
            else -> throw IllegalArgumentException("unexpected thought type '${thoughts::class.java.name}'")
        }
    }

    override fun addThought(type: IContentType, data: ByteArray) {
        when (type) {
            ContentType.Text -> addThought(Unsynced(type = ContentType.Text, data = data))
        }
    }

    override fun addAll(vararg thoughts: IThought) {
        thoughts.forEach { addThought(it) }
    }

    override fun removeThought(thought: IThought) {
        when (thought) {
            is Thought.ThoughtWithContent -> {
                synchronized(thoughts) { thoughts.remove(thought) }
                if (thought is Synced) addThought(Undeleted(thought.UUID))
            }
            is Undeleted -> {
                synchronized(thoughts) {
                    val doomedThoughts = thoughts.filter { it is Synced && it.UUID == thought.UUID }
                    thoughts.removeAll(doomedThoughts)
                }
                synchronized(undeletedThoughts) { undeletedThoughts.remove(thought) }
            }
            else -> throw IllegalArgumentException("unexpected thought type '${thoughts::class.java.name}'")
        }
    }

    override fun peek(): IThoughtWithContent? {
        synchronized(thoughts) {
            return thoughts.firstOrNull()
        }
    }

    override fun pop(): IThoughtWithContent? {
        val removed = synchronized(thoughts) { thoughts.pollFirst() }
        return removed?.also {
            when (it) {
                //if the removed thought was synchronized, we need to tell the server about it
                is Synced -> addThought(Undeleted(it.UUID))

                //if it wasn't, the server doesn't care
                is Unsynced -> {
                }
                else -> throw IllegalArgumentException("unexpected thought type ${removed::class.java.name}")
            }
        }
    }

    override fun contains(thought: IThought): Boolean {
        return when (thought) {
            is Thought.ThoughtWithContent -> synchronized(thoughts) { thoughts.contains(thought) }
            is Undeleted -> synchronized(undeletedThoughts) { undeletedThoughts.contains(thought) }
            else -> throw IllegalArgumentException("unexpected thought type '${thoughts::class.java.name}'")
        }
    }

    override fun containsAll(elements: Collection<IThought>): Boolean {
        for (e in elements) {
            if (!this.contains(e)) return false
        }
        return true
    }

    override fun iterator(): Iterator<IThought> {
        return Iterators.concat(thoughts.iterator(), undeletedThoughts.iterator())
    }

    override fun forEach(f: (IThought) -> Unit) {
        synchronized(thoughts) { thoughts.forEach(f) }
        synchronized(undeletedThoughts) { undeletedThoughts.forEach(f) }
    }

    override fun mergeWith(syncResponse: ISyncResponse): ThoughtSet {
        if (DEBUG_MERGE) {
            Log.d(TAG, "<<BEFORE MERGE>>\n\t${persistableEncoding()}")
            Log.d(TAG, "\n\t" +
                "merge:new=${syncResponse.new.map { it.persistableEncoding }}\n\t" +
                "merge:acknowledged=${syncResponse.acknowledged}\n\t" +
                "merge:deleted=${syncResponse.deleted}"
            )
        }

        synchronized(thoughts) {
            syncResponse.new.forEach { addThought(it) }

            syncResponse.acknowledged
                .map {
                    unsyncedThoughts[it.key]?.elevate(it.value)
                        ?: throw IllegalStateException("cannot find acknowledged thought with key ${it.key} in local collection: $unsyncedThoughts")
                }
                .forEach {
                    thoughts.remove(it) //removes the "Unsynced" version
                    thoughts.add(it) //replaces it with the "Synced" version
                }

            syncResponse.deleted.forEach { removeThought(Undeleted(it)) }
        }
        if (DEBUG_MERGE) {
            Log.d(TAG, "<<AFTER MERGE>>\n\t${persistableEncoding()}")
        }
        return this
    }

    override fun toString(): String {
        return synchronized(thoughts) {
            "ThoughtSet(\n${thoughts.map { it.toString() + "\n" }})"
        }
    }


}