package eu.cs_syd.intray.common.service.responses.errors.causes

import eu.cs_syd.intray.common.service.responses.IServerError

interface IRequestDenied : IServerError