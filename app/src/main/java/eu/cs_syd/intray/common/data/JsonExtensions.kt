package eu.cs_syd.intray.common.data

import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced

inline fun json(content: JsonHelper.() -> Unit): JsonObject {
    val aux = JsonHelper()
    aux.content()
    return aux.jsonObject
}

fun String.toJson(): JsonObject {
    return JsonParser().parse(this).asJsonObject
}

class JsonHelper {
    val jsonObject = JsonObject()

    infix fun String.to(value: Boolean) = jsonObject.addProperty(this, value)
    infix fun String.to(value: String) = jsonObject.addProperty(this, value)
    infix fun String.to(value: Number) = jsonObject.addProperty(this, value)
    infix fun String.to(value: Char) = jsonObject.addProperty(this, value)
    infix fun String.to(value: JsonHelper) = jsonObject.add(this, value.jsonObject)
    infix fun String.to(value: JsonObject) = jsonObject.add(this, value)
    infix fun String.to(value: JsonArray) = jsonObject.add(this, value)
    infix fun <K, V> String.to(value: Map<K, V>) {
        val gson = Gson()
        jsonObject.add(this, gson.toJsonTree(value.map { it.key to jsonRepresentation(it.value) }.toMap()))
    }

    infix fun String.to(value: Any?) {
        value?.let {
            throw IllegalArgumentException("${it::class.java.name} is not a supported json value")
        } ?: jsonObject.add(this, JsonNull.INSTANCE)
    }

    fun jsonRepresentation(value: Unsynced) = value.syncRepresentation
    fun jsonRepresentation(value: Any?): JsonElement {
        return when (value) {
            null -> JsonNull.INSTANCE
            is Unsynced -> value.syncRepresentation
            else -> throw IllegalArgumentException("Don't know how to translate ${value::class.java}")
        }

    }
}