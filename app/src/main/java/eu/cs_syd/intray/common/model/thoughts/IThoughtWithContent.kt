package eu.cs_syd.intray.common.model.thoughts

import java.util.Date

interface IThoughtWithContent : IThought {
    val type: IContentType
    val data: ByteArray

    val timestampCreated: Date
}