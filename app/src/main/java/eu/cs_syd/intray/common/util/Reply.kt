package eu.cs_syd.intray.common.util;

/**Auxiliary class until [kotlin.Result] is ready for deployment
 * Keep an eye on [https://github.com/Kotlin/KEEP/blob/master/proposals/stdlib/result.md]*/
sealed class Reply<in T> {
    open class OK<T>(val result: T) : Reply<T>()
    open class Fail(val cause: Throwable) : Reply<Any?>()
}