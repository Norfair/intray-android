package eu.cs_syd.intray.common.model.thoughts

interface IDeletedThought : IThought {
    val UUID: String
}