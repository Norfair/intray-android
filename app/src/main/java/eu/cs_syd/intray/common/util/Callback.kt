package eu.cs_syd.intray.common.util

class Callback<T>(
    val onResponse: (T) -> Unit,
    val onError: (Throwable) -> Unit
)