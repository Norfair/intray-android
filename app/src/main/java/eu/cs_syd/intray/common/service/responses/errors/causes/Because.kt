package eu.cs_syd.intray.common.service.responses.errors.causes

/** extends [RuntimeException] so it can take arbitrary [Throwable]s as an error message*/
sealed class Because : IServerErrorCause, RuntimeException {
    constructor(message: String) : super(message)
    constructor(message: String, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable) : super(cause)

    class ServerRejectsLogin : IServerRejectsLogin, Because {
        constructor(message: String) : super(message)
        constructor(message: String, cause: Throwable?) : super(message, cause)
        constructor(cause: Throwable) : super(cause)
    }

    class UserAlreadyExists : IUserAlreadyExists, Because {
        constructor(message: String) : super(message)
        constructor(message: String, cause: Throwable?) : super(message, cause)
        constructor(cause: Throwable) : super(cause)
    }

    object Unknown : Because("there was an unknown error") {
    }
}