package eu.cs_syd.intray.common.service.requests

interface IRequestDispatcher<Params> {
    /**@return true, if the request was dispatched, false otherwise*/
    fun makeRequest(req: IServerRequest, params: Params): Boolean
}