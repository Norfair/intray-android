package eu.cs_syd.intray.common.service.responses.errors.causes

/**when a user attempts to log in with invalid credentials
 * or if his login has times out*/
interface IServerRejectsLogin : IServerErrorCause