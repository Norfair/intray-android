package eu.cs_syd.intray.common.data.persistence

class IllegalEncoding : IllegalArgumentException {
    constructor(msg: String) : super(msg)
    constructor(msg: String, cause: Throwable) : super(msg, cause)
    constructor(cause: Throwable) : super(cause)
}