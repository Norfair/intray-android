package eu.cs_syd.intray.common.model.thoughts

import eu.cs_syd.intray.synchronisation.service.ISyncResponse

/**A collection of [IThought]s.
 * Implements some kind of order that determines when what item is processed.*/
interface IThoughtSet : Collection<IThought> {

    /**@return an abstraction of the collection that can be used to recreate it*/
    fun persistableEncoding(): String

    /**wraps the [data] into an appropriate [IThought], depending on the [type]
     * and adds it to the collection*/
    fun addThought(type: IContentType, data: ByteArray)

    /**adds an [IThought] to this collection*/
    fun addThought(thought: IThought)

    /**adds all [IThought]s to this collection*/
    fun addAll(vararg thoughts: IThought)

    /**removes an [IThought] contained in this set*/
    fun removeThought(thought: IThought)

    /**@return the "next" [IThought] to be processed without removing it or null if there is no "next" element*/
    fun peek(): IThoughtWithContent?

    /**@return the "next" [IThought] to be processed, removing it or null if there is no "next" element*/
    fun pop(): IThoughtWithContent?

    /**iterates over all elements, invoking [f] with each*/
    fun forEach(f: (IThought) -> Unit)

    /**updates the collection based on the [syncResponse]
     * @return  the resulting collection*/
    fun mergeWith(syncResponse: ISyncResponse): IThoughtSet
}