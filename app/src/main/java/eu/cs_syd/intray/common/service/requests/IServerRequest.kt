package eu.cs_syd.intray.common.service.requests

/** encapsulates a request to be sent over the web*/
interface IServerRequest {
    /**visitor pattern*/
    fun <ParamType, ReturnType> accept(visitor: IServerRequestVisitor<in ParamType, out ReturnType>, params: ParamType): ReturnType
}