package eu.cs_syd.intray.common.service.requests

import eu.cs_syd.intray.account.service.IAccountGetRequest
import eu.cs_syd.intray.login.service.ILoginRequest
import eu.cs_syd.intray.registration.service.IRegistrationRequest
import eu.cs_syd.intray.synchronisation.service.ISyncRequest

/** A visitor to facilitate parsing the request to a different kind of business object. */
interface IServerRequestVisitor<in Params, out ReturnValue> {
    fun visit(req: IRegistrationRequest, params: Params): ReturnValue
    fun visit(req: ILoginRequest, params: Params): ReturnValue
    fun visit(req: IAccountGetRequest, params: Params): ReturnValue
    fun visit(req: ISyncRequest, params: Params): ReturnValue
}