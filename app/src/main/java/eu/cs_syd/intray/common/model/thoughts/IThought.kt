package eu.cs_syd.intray.common.model.thoughts

interface IThought : ISyncCandidate {
    /**A string from which the [IThought] can be recreated.*/
    val persistableEncoding: String
}