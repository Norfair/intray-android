package eu.cs_syd.intray.common.service.responses.errors.causes

import eu.cs_syd.intray.common.service.responses.errors.AbstractServerError

class RequestDenied(cause: Because) : IRequestDenied, AbstractServerError(cause)