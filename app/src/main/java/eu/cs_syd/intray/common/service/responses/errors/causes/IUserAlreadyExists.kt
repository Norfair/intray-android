package eu.cs_syd.intray.common.service.responses.errors.causes

interface IUserAlreadyExists : IServerErrorCause {
}