package eu.cs_syd.intray.common.service.responses

/**parent interface to both [IValidServerResponse] and [IServerError]*/
interface IServerResponse