package eu.cs_syd.intray.account.ui.contentviews

import androidx.fragment.app.Fragment
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent

abstract class AbstractContentView : Fragment() {
    abstract fun displayThought(thought: IThoughtWithContent)
}