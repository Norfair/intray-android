package eu.cs_syd.intray.account.service

import eu.cs_syd.intray.user.model.IUserInformation

data class AccountGetResponse(
    override val info: IUserInformation
) : IAccountGetResponse