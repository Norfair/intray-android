package eu.cs_syd.intray.account.ui

import android.content.Intent
import android.view.View
import androidx.fragment.app.Fragment
import eu.cs_syd.intray.app.appViewModels
import eu.cs_syd.intray.login.ui.LoginActivity
import eu.cs_syd.intray.user.service.UserManager

abstract class AbstractAccountFragment : Fragment() {
    protected val userManager: UserManager by appViewModels()

    /**logs the user out and then starts a [LoginActivity] s.t. the new user can log in*/
    protected fun onLogoutButtonPressed(view: View) {
        //log out
        userManager.onUserLogout(userManager.getActiveUser())

        //user is now offline ask for login
        //TODO: if/when login-less mode is added, adjust this section
        val intent = Intent(activity, LoginActivity::class.java)
        startActivity(intent)
    }

    abstract fun doUpdate()

}