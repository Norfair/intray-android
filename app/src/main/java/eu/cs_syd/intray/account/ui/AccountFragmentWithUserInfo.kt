package eu.cs_syd.intray.account.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.cs_syd.intray.R
import eu.cs_syd.intray.common.util.Callback
import eu.cs_syd.intray.user.model.IUserInformation
import eu.cs_syd.intray.user.service.UserManager
import kotlinx.android.synthetic.main.fragment_account_with_user_info.adminText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.createdText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.lastLoginText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.usernameText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.uuidText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.view.logoutButton
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.uiThread

class AccountFragmentWithUserInfo : AbstractAccountFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_account_with_user_info, container, false)
        view.logoutButton.setOnClickListener(this::onLogoutButtonPressed)
        return view
    }

    override fun doUpdate() {
        //TODO: replace this method with live data interactions
        val au = userManager.getActiveUser()

        if (au.name == UserManager.DEFAULT_USER_NAME) {
            doAsync { uiThread { activity?.longToast("you are offline") } }
            return
        }

        usernameText.text = au.name

        userManager.getUserInfo(au, Callback(
            onResponse = { updateView(it) },
            onError = { doAsync { uiThread { activity?.longToast("error: $it") } } }
        ))
    }

    private fun updateView(data: IUserInformation) {
        usernameText.text = data.name
        uuidText.text = data.uuid
        adminText.text = if (data.isAdmin) "YES" else "NO"

        createdText.text = data.created.toString()
        lastLoginText.text = data.lastLogin?.toString() ?: "-"
    }
}