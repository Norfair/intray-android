package eu.cs_syd.intray.account.service

import eu.cs_syd.intray.common.service.requests.IServerRequest
import eu.cs_syd.intray.user.model.IUser

interface IAccountGetRequest : IServerRequest {
    val user: IUser
}