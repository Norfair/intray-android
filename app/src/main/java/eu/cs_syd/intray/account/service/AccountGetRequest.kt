package eu.cs_syd.intray.account.service

import eu.cs_syd.intray.common.service.requests.IServerRequestVisitor
import eu.cs_syd.intray.user.model.IUser

data class AccountGetRequest(override val user: IUser) : IAccountGetRequest {
    override fun <ParamType, ReturnType> accept(visitor: IServerRequestVisitor<ParamType, ReturnType>, params: ParamType): ReturnType {
        return visitor.visit(this, params)
    }
}