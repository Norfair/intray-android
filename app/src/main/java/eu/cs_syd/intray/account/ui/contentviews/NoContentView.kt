package eu.cs_syd.intray.account.ui.contentviews

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.cs_syd.intray.R
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import kotlinx.android.synthetic.main.fragment_process_component_text_view.view.content

class NoContentView : AbstractContentView() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val fragmentView = inflater.inflate(R.layout.fragment_process_component_text_view, container, false)
        fragmentView.content.text = Html.fromHtml(getString(R.string.process_intrayIsEmpty))
        return fragmentView
    }

    override fun displayThought(thought: IThoughtWithContent) {
        throw IllegalStateException("this view cannot display any thoughts")
    }
}