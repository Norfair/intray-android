package eu.cs_syd.intray.account.ui.contentviews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.cs_syd.intray.R
import eu.cs_syd.intray.common.model.thoughts.IThoughtWithContent
import kotlinx.android.synthetic.main.fragment_process_component_text_view.content
import kotlinx.android.synthetic.main.fragment_process_component_text_view.view.content

class TextContentView : AbstractContentView() {
    @Volatile
    private var displayedData: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_process_component_text_view, container, false)
        displayedData?.let { view.content.text = it }
        return view
    }

    override fun displayThought(thought: IThoughtWithContent) {
        displayedData = String(thought.data, Charsets.UTF_8)
        content?.text = displayedData
    }
}