package eu.cs_syd.intray.account.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import eu.cs_syd.intray.R
import kotlinx.android.synthetic.main.fragment_account_with_user_info.usernameText
import kotlinx.android.synthetic.main.fragment_account_with_user_info.view.logoutButton

class AccountFragmentWithoutUserInfo : AbstractAccountFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_account_without_user_info, container, false)
        view.logoutButton.setOnClickListener(this::onLogoutButtonPressed)
        return view
    }

    override fun doUpdate() {
        usernameText.text = userManager.getActiveUser().name
    }
}