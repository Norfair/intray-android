package eu.cs_syd.intray.account.service

import eu.cs_syd.intray.common.service.responses.IValidServerResponse
import eu.cs_syd.intray.user.model.IUserInformation

interface IAccountGetResponse : IValidServerResponse {
    val info: IUserInformation
}