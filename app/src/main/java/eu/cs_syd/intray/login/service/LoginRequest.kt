package eu.cs_syd.intray.login.service

import eu.cs_syd.intray.common.service.requests.IServerRequestVisitor

data class LoginRequest(override val username: String, override val password: String) : ILoginRequest {
    override fun <ParamType, ReturnType> accept(visitor: IServerRequestVisitor<ParamType, ReturnType>, params: ParamType): ReturnType {
        return visitor.visit(this, params)
    }
}