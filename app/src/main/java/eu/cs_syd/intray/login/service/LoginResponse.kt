package eu.cs_syd.intray.login.service

import eu.cs_syd.intray.user.model.ICredentials

data class LoginResponse(
    override val serverName: String,
    override val credentials: ICredentials
) : ILoginResponse