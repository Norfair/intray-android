package eu.cs_syd.intray.login.service

import eu.cs_syd.intray.common.service.responses.IValidServerResponse
import eu.cs_syd.intray.user.model.ICredentials

interface ILoginResponse : IValidServerResponse {
    val serverName: String
    val credentials: ICredentials
}