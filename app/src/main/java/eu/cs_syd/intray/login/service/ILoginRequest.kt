package eu.cs_syd.intray.login.service

import eu.cs_syd.intray.common.service.requests.IServerRequest

interface ILoginRequest : IServerRequest {
    val username: String
    val password: String
}