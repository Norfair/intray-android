package eu.cs_syd.intray.login.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Response
import eu.cs_syd.intray.R
import eu.cs_syd.intray.app.appViewModels
import eu.cs_syd.intray.common.service.responses.IServerError
import eu.cs_syd.intray.common.service.responses.errors.causes.Because
import eu.cs_syd.intray.config.WebConfig
import eu.cs_syd.intray.login.service.ILoginResponse
import eu.cs_syd.intray.login.service.LoginRequest
import eu.cs_syd.intray.network.volley.ResponseListeners
import eu.cs_syd.intray.network.volley.VolleyRequestDispatcher
import eu.cs_syd.intray.registration.ui.RegistrationActivity
import eu.cs_syd.intray.user.model.User
import eu.cs_syd.intray.user.service.UserManager
import kotlinx.android.synthetic.main.activity_login.loginButton
import kotlinx.android.synthetic.main.activity_login.passwordEditText
import kotlinx.android.synthetic.main.activity_login.registerButton
import kotlinx.android.synthetic.main.activity_login.usernameEditText
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import org.jetbrains.anko.uiThread


class LoginActivity : AppCompatActivity() {
    private val userManager: UserManager by appViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginButton.setOnClickListener(this::onLoginButtonPressed)
        registerButton.setOnClickListener(this::onRegisterButtonPressed)
    }

    /** disables the back button*/
    override fun onBackPressed() {
        //do nothing
    }

    /**checks user input and either invokes [makeLoginRequest] or displays an error message*/
    private fun onLoginButtonPressed(view: View) {
        val username = usernameEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (username.isEmpty()) doAsync {
            uiThread {
                it.toast("please enter a username")
            }
        }
        else if (password.isEmpty()) doAsync {
            uiThread {
                it.toast("please enter a password")
            }
        }
        else makeLoginRequest(username, password)
    }

    /**starts a [RegisterationActivity] and communicates the username and password to it*/
    private fun onRegisterButtonPressed(view: View) {
        val intent = Intent(this, RegistrationActivity::class.java)

        intent.putExtra("username", usernameEditText.text.toString())
        intent.putExtra("password", passwordEditText.text.toString())

        startActivity(intent)
    }

    private var activeLoginRequest = false
    /**Encapsulates a login request and passes it on to the [VolleyRequestDispatcher]*/
    private fun makeLoginRequest(username: String, password: String) {
        if (activeLoginRequest) return
        activeLoginRequest = true
        val req = LoginRequest(username, password)
        val params = ResponseListeners(
            onResponse = Response.Listener {
                when (it) {
                    is IServerError -> toastLoginFailure(it.cause)
                    is ILoginResponse -> {
                        val server = it.serverName
                        val creds = it.credentials
                        val user = User(username, server, creds)
                        userManager.onUserLogin(user)
                        finish()
                    }
                    else -> throw IllegalArgumentException("unexpected response type `${it::class.java.name}`")
                }
                activeLoginRequest = false
            },
            onError = Response.ErrorListener { volleyError ->
                doAsync { uiThread { longToast("unexpected error: $volleyError") } }
                println(volleyError.stackTrace)
            }
        )
        val dispatcher = WebConfig.getRequestDispatcher() as VolleyRequestDispatcher
        val isAccepted = dispatcher.makeRequest(req, params)
        if (!isAccepted) {
            activeLoginRequest = false
            toastConnectivityIssues()
        }
    }

    private fun toastConnectivityIssues() {
        doAsync { uiThread { toast("please connect your device to the internet") } }
    }

    private fun toastLoginFailure(reason: Throwable) {
        val msg = if (reason is Because.ServerRejectsLogin) "please check your username and password"
        else reason.message ?: "sorry, there was an error logging you in"

        doAsync { uiThread { toast(msg) } }
    }
}