package eu.cs_syd.intray.common

import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ServiceTestRule
import eu.cs_syd.intray.app.ui.MainActivity
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
abstract class AndroidIntegrationTest {

    @get:Rule
    var mainActivityRule = activityScenarioRule<MainActivity>()

    @get:Rule
    val serviceRule = ServiceTestRule()
}