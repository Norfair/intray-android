package eu.cs_syd.intray.synchronisation

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNotNull
import assertk.assertions.isTrue
import eu.cs_syd.intray.common.AndroidIntegrationTest
import eu.cs_syd.intray.common.model.thoughts.ContentType.Text
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.common.model.thoughts.ThoughtSet
import eu.cs_syd.intray.network.retrofit.DebugServer
import eu.cs_syd.intray.network.retrofit.DebugServer.testUser
import eu.cs_syd.intray.synchronisation.service.SyncRequest
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import java.nio.charset.Charset

class SynchronisationIT : AndroidIntegrationTest() {

    @Before
    fun clearItems() {
        DebugServer.clearItems()
    }

    @Test
    fun sync_simple() {
        val request = SyncRequest(testUser, ThoughtSet(
            Unsynced(
                type = Text,
                data = "Hello World".toByteArray()
            )
        ))

        val response = runBlocking { DebugServer.trySync(request).await() }

        assertThat(response.isSuccessful).isTrue()

        val addedThoughts = response.body()!!.get("client-added").asJsonObject
        assertThat(addedThoughts.size()).isEqualTo(1)

        val addedItemId = addedThoughts.entrySet().first().value.asString
        val thought = DebugServer.retrieveItemById(addedItemId)

        assertThat(thought).isNotNull()
        thought!!
        assertThat(thought.UUID).isEqualTo(addedItemId)
        assertThat(thought.data)
            .transform { it.toString(Charset.defaultCharset()) }
            .isEqualTo("Hello World")
    }

    @Test
    fun sync_clearItems() {
        var request = SyncRequest(testUser, ThoughtSet(
            Unsynced(
                type = Text,
                data = "Hello World".toByteArray()
            ),
            Unsynced(
                type = Text,
                data = "Hakuna Matata".toByteArray()
            )
        ))
        val setup = runBlocking { DebugServer.trySync(request).await() }

        request = emptySyncRequest()
        val before = runBlocking { DebugServer.trySync(request).await() }.body()?.get("server-added")?.asJsonObject
            ?: throw IllegalStateException("sync request failed unexpectedly")
        assertThat(before.size()).isEqualTo(2)

        DebugServer.clearItems()

        request = emptySyncRequest()
        val after = runBlocking { DebugServer.trySync(request).await() }.body()?.get("server-added")?.asJsonObject
            ?: throw IllegalStateException("sync request failed unexpectedly")
        assertThat(after.size()).isEqualTo(0)
    }

    private fun emptySyncRequest() = SyncRequest(testUser, ThoughtSet())

}