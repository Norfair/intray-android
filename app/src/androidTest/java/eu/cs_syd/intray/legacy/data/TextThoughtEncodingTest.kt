package eu.cs_syd.intray.legacy.data

//
//import com.google.gson.JsonArray
//import eu.cs_syd.intray.editing.data.AndroidBase64EncoderDecoder
//import eu.cs_syd.intray.app.service.Encoder
//import eu.cs_syd.intray.common.model.thoughts.ContentType
//import eu.cs_syd.intray.common.model.thoughts.Thought
//import eu.cs_syd.intray.common.data.json
//import eu.cs_syd.intray.network.retrofit.DebugServer
//import io.kotlintest.KTestJUnitRunner
//import io.kotlintest.properties.Gen
//import io.kotlintest.properties.forAll
//import io.kotlintest.specs.FunSpec
//import kotlinx.coroutines.runBlocking
//import org.junit.runner.RunWith
//
//@RunWith(KTestJUnitRunner::class)
//class TextThoughtEncoding : FunSpec() {
//    init {
//        test("text encoded through [AndroidBase64EncoderDecoder] can be synced to the server") {
//            Encoder.base64EncoderDecoder = AndroidBase64EncoderDecoder()
//            val jwt = DebugServer.JWT
//            val canSync: (String) -> Boolean = { canSync(jwt, it) }
//            forAll(Gen.string(), canSync)
//        }
//    }
//
//    private val syncRequestTemplate = json {
//        "undeleted" to JsonArray()
//        "synced" to JsonArray()
//    }
//
//    private fun canSync(jwt: String, msg: String): Boolean {
//        val req = syncRequestTemplate.deepCopy()
//
//        val unsynced = JsonArray()
//        unsynced.add(Thought.Unsynced(
//            type = ContentType.Text,
//            data = msg.toByteArray()
//        ).syncRepresentation)
//
//        req.add("unsynced", unsynced)
//
//        val resp = runBlocking {
//            DebugServer.trySync(jwt, req).await()
//        }
//
//        return resp.isSuccessful
//    }
//}