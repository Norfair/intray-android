package eu.cs_syd.intray.synchronisation.service

import eu.cs_syd.intray.app.service.Encoder.fromBase64
import eu.cs_syd.intray.common.data.toJson
import eu.cs_syd.intray.common.model.thoughts.ContentType
import eu.cs_syd.intray.common.model.thoughts.IThought
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.common.model.thoughts.ThoughtSet
import eu.cs_syd.intray.config.DataConfig
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.synced
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.undeleted
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.unsynced
import eu.cs_syd.intray.user.model.User
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

class SyncResponseTest {
    private infix fun SyncRequest.answeredWith(response: String): SyncResponse {
        return SyncResponse(this, response.toJson())
    }

    private lateinit var user: User

    private fun syncRequest(vararg thoughts: IThought) = SyncRequest(user, ThoughtSet(*thoughts))

    @BeforeEach
    fun setup() {
        user = mock(User::class.java)
    }

    @Test
    fun `empty response`() {
        val request = syncRequest()

        lateinit var response: SyncResponse
        assertThatCode {
            response = request answeredWith """
                {
                    "client-added": {},
                    "server-deleted": [],
                    "server-added": {},
                    "client-deleted": []
                }
            """.trimIndent()
        }.doesNotThrowAnyException()

        assertThat(response.acknowledged).isEmpty()
        assertThat(response.deleted).isEmpty()
        assertThat(response.new).isEmpty()
    }

    @Test
    fun `client created something`() {
        val request = syncRequest(unsynced("Hello world"))
        assertThat(request.thoughts).hasSize(1)
        assertThat(request.thoughts.peek()).isInstanceOf(Unsynced::class.java)

        val response = request answeredWith """
            {
                "client-added": {"1" : "my-uuid"},
                "server-deleted": [],
                "server-added": {},
                "client-deleted": []
            }
        """.trimIndent()

        val result = request.thoughts.mergeWith(response)

        assertThat(result).hasSize(1)
        assertThat(result.peek())
            .isInstanceOf(Synced::class.java)
            .hasFieldOrPropertyWithValue("UUID", "my-uuid")
    }

    @Test
    fun `acknowledged client-side delete`() {
        val request = syncRequest(undeleted("my-uuid"))
        assertThat(request.thoughts).hasSize(1)
        assertThat(request.thoughts.peek()).isNull() //undeleted not offered for processing

        val response = request answeredWith """
            {
                "client-added": {},
                "server-deleted": [],
                "server-added": {},
                "client-deleted": ["my-uuid"]
            }
        """.trimIndent()

        val result = request.thoughts.mergeWith(response)

        assertThat(result).hasSize(0)
    }

    @Test
    fun `server-side delete removes client-side synchronised thoughts`() {
        val request = syncRequest(synced("my-uuid", "Hello World"))
        assertThat(request.thoughts).hasSize(1)
        assertThat(request.thoughts.peek()).isInstanceOf(Synced::class.java)

        val response = request answeredWith """
            {
                "client-added": {},
                "server-deleted": ["my-uuid"],
                "server-added": {},
                "client-deleted": []
            }
        """.trimIndent()

        val result = request.thoughts.mergeWith(response)

        assertThat(result).isEmpty()
    }

    @Test
    fun `server-side addition integrated into local set`() {
        val request = syncRequest()
        assertThat(request.thoughts).isEmpty()

        val response = request answeredWith """
            {
                "client-added": {},
                "server-deleted": [],
                "server-added": {
                    "uuid-of-addition": {
                        "contents": {
                            "data": "SGVsbG8gV29ybGQh",
                            "type": "text"
                        },
                        "created": "2018-02-10T00:00:42.123Z"
                    }
                },
                "client-deleted": []
            }
        """.trimIndent()

        val result = request.thoughts.mergeWith(response)

        assertThat(result).hasSize(1)
        assertThat(result.peek())
            .isInstanceOf(Synced::class.java)
            .hasFieldOrPropertyWithValue("UUID", "uuid-of-addition")
            .hasFieldOrPropertyWithValue("type", ContentType.Text)
            .hasFieldOrPropertyWithValue("data", fromBase64("SGVsbG8gV29ybGQh"))
            .hasFieldOrPropertyWithValue("timestampCreated", DataConfig.dateFormat.parse("2018-02-10T00:00:42.123Z"))
    }
}


