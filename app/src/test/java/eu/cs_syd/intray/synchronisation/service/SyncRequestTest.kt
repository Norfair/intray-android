package eu.cs_syd.intray.synchronisation.service

import com.google.gson.Gson
import eu.cs_syd.intray.common.data.toJson
import eu.cs_syd.intray.common.model.thoughts.ThoughtSet
import eu.cs_syd.intray.parse
import eu.cs_syd.intray.testutils.data.TestDateStrings.on_2001_04_10
import eu.cs_syd.intray.testutils.data.TestDateStrings.on_2017_01_10
import eu.cs_syd.intray.testutils.data.TestDateStrings.on_2020_04_10_A
import eu.cs_syd.intray.testutils.data.TestDateStrings.on_2020_04_10_B
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.synced
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.undeleted
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.unsynced
import eu.cs_syd.intray.user.model.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

internal class SyncRequestTest {

    private val gson = Gson()

    @Test
    fun `empty sync request json format`() {
        val user = mock(User::class.java)
        val thoughts = ThoughtSet()
        val expected = gson.parse(
            """
                {
                "undeleted":[],
                "added":{},
                "synced":[]
                }
            """.trimIndent()
        )

        val syncRequest = SyncRequest(user, thoughts)
        val actual = gson.parse(syncRequest.jsonRepresentation)

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `sync request with two new thoughts`() {
        val user = mock(User::class.java)
        val unsyncedA = unsynced("Hello World", on_2020_04_10_A)
        val unsyncedB = unsynced("Hakuna Matata", on_2020_04_10_B)
        val thoughts = ThoughtSet(unsyncedA, unsyncedB)

        val request = SyncRequest(user, thoughts)

        assertThat(request.jsonRepresentation.toJson()).isEqualTo("""
            {
                "undeleted":[],
                "added":{
                    "1" : ${unsyncedA.syncRepresentation},
                    "2" : ${unsyncedB.syncRepresentation}
                },
                "synced":[]
            }
        """.toJson())
    }

    @Test
    fun `sync request with two deleted thoughts`() {
        val user = mock(User::class.java)
        val deletedA = undeleted("foo")
        val deletedB = undeleted("bar")
        val thoughts = ThoughtSet(deletedA, deletedB)

        val request = SyncRequest(user, thoughts)

        assertThat(request.jsonRepresentation.toJson()).isEqualTo("""
            {
                "undeleted":[
                    ${deletedA.syncRepresentation},
                    ${deletedB.syncRepresentation}
                ],
                "added":{},
                "synced":[]
            }
        """.toJson())
    }

    @Test
    fun `sync request with two synced thoughts`() {
        val user = mock(User::class.java)
        val syncedA = synced("foo", "Hello World", on_2020_04_10_A)
        val syncedB = synced("bar", "Hakuna Matata", on_2020_04_10_B)
        val thoughts = ThoughtSet(syncedA, syncedB)

        val request = SyncRequest(user, thoughts)

        assertThat(request.jsonRepresentation.toJson()).isEqualTo("""
            {
                "undeleted":[],
                "added":{},
                "synced":[
                    ${syncedA.syncRepresentation},
                    ${syncedB.syncRepresentation}
                ]
            }
        """.toJson())
    }

    @Test
    fun `sync request with all kinds of thoughts`() {
        val user = mock(User::class.java)
        val unsyncedA = unsynced("Testing", on_2020_04_10_A)
        val unsyncedB = unsynced("One two three", on_2020_04_10_B)
        val deletedA = undeleted("bye")
        val deletedB = undeleted("byebye")
        val syncedA = synced("foo", "Hello World", on_2001_04_10)
        val syncedB = synced("bar", "Hakuna Matata", on_2017_01_10)
        val thoughts = ThoughtSet(syncedA, deletedA, syncedB, unsyncedA, deletedB, unsyncedB)

        val request = SyncRequest(user, thoughts)

        assertThat(request.jsonRepresentation.toJson()).isEqualTo("""
            {
                "undeleted":[
                    ${deletedA.syncRepresentation},
                    ${deletedB.syncRepresentation}
                ],
                "added":{
                    "1" : ${unsyncedA.syncRepresentation},
                    "2" : ${unsyncedB.syncRepresentation}
                },
                "synced":[
                    ${syncedA.syncRepresentation},
                    ${syncedB.syncRepresentation}
                ]
            }
        """.toJson())
    }
}