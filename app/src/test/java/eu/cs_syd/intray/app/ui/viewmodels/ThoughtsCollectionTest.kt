package eu.cs_syd.intray.app.ui.viewmodels

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import eu.cs_syd.intray.testutils.data.TestThoughtGenerator.unsynced
import eu.cs_syd.intray.testutils.getOrAwaitValue
import org.assertj.core.api.AssertionsForClassTypes.assertThat
import org.junit.Rule
import org.junit.Test

internal class ThoughtsCollectionTest {
    /**
     * runs all Architecture Components-related background jobs in the same thread
     */
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `adding or removing item sets next thought`() {
        val thoughts = ThoughtsCollection(mock<Application>())
        val newThought = unsynced("Hello World")

        thoughts.addThought(newThought)

        val nextAfterAdding = thoughts.nextThought.getOrAwaitValue()
        assertThat(nextAfterAdding).isEqualTo(newThought)

        thoughts.removeNextThought()

        val nextAfterRemoving = thoughts.nextThought.getOrAwaitValue()
        assertThat(nextAfterRemoving).isNull()
    }


}