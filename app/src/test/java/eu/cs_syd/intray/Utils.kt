package eu.cs_syd.intray

import com.google.gson.Gson
import com.google.gson.JsonObject
import eu.cs_syd.intray.editing.data.IBase64EncoderDecoder
import java.util.Base64

object Utils {
    val javaBase64EncoderDecoder = object : IBase64EncoderDecoder {
        val encoder = Base64.getEncoder()
        val decoder = Base64.getDecoder()

        override fun toBase64(data: ByteArray): String {
            return encoder.encodeToString(data)
        }

        override fun fromBase64(encodedData: String): ByteArray {
            return decoder.decode(encodedData)
        }
    }
}


fun Gson.parse(jsonString: String): JsonObject {
    return this.fromJson(jsonString, JsonObject::class.java)
}