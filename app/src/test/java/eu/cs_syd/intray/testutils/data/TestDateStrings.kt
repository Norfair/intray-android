package eu.cs_syd.intray.testutils.data

object TestDateStrings {
    val on_2020_04_10_A = "2020-04-10T18:28:25.589Z"
    val on_2020_04_10_B = "2020-04-10T19:24:125.307Z"
    val on_2017_01_10 = "2017-01-10T12:13:22.365Z"
    val on_2001_04_10 = "2001-04-10T09:16:44.100Z"
}