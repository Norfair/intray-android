package eu.cs_syd.intray.testutils.data

import eu.cs_syd.intray.Utils
import eu.cs_syd.intray.app.service.Encoder
import eu.cs_syd.intray.common.model.thoughts.ContentType.Text
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Undeleted
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced.Ack
import eu.cs_syd.intray.config.DataConfig
import eu.cs_syd.intray.testutils.data.TestDateStrings.on_2020_04_10_A

object TestThoughtGenerator {
    init {
        Encoder.base64EncoderDecoder = Utils.javaBase64EncoderDecoder
    }

    fun synced(uuid: String, content: String, dateCreatedAt: String = on_2020_04_10_A): Synced {
        return unsynced(content, dateCreatedAt).elevate(Ack(uuid))
    }

    fun unsynced(content: String, dateCreatedAt: String = on_2020_04_10_A): Unsynced {
        return Unsynced(
            type = Text,
            data = content.toByteArray(),
            timestampCreated = DataConfig.dateFormat.parse(dateCreatedAt)
        )
    }

    fun undeleted(uuid: String): Undeleted {
        return Undeleted(uuid)
    }
}