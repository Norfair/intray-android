package eu.cs_syd.intray.testutils.data

object TestDataEncodings {
    const val helloWorld = "SGVsbG8gV29ybGQ=" // "Hello World"
    const val hakunaMatata = "SGFrdW5hIE1hdGF0YQ==" // "Hakuna Matata"
}