package eu.cs_syd.intray.common.model.thoughts

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import eu.cs_syd.intray.Utils.javaBase64EncoderDecoder
import eu.cs_syd.intray.app.service.Encoder
import eu.cs_syd.intray.common.model.thoughts.ContentType.Text
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Undeleted
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced.Ack
import eu.cs_syd.intray.synchronisation.service.SyncResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ThoughtSetTests {

    @BeforeEach
    fun setup() {
        Encoder.base64EncoderDecoder = javaBase64EncoderDecoder
    }

    @Test
    fun `locally deleting a thought replaces it with an undeleted thought`() {
        //given
        val uuid = "foo"
        val synced = mock<Synced> {
            on(mock.UUID) doReturn uuid
        }

        val thoughts = ThoughtSet()
        thoughts.addThought(synced)

        //when
        thoughts.removeThought(synced)

        //then
        assertThat(thoughts.size).isEqualTo(1)
        thoughts.forEach { assertThat(it).isInstanceOf(Undeleted::class.java) }
    }

    @Test
    fun `new thoughts received from server are added to the local set`() {
        //given
        val data = "some data"
        val synced = mock<Synced> {
            on(mock.data) doReturn data.toByteArray()
        }
        val syncResponse = mock<SyncResponse> {
            on(mock.new) doReturn setOf(synced)
        }

        val thoughts = ThoughtSet()

        //when
        thoughts.mergeWith(syncResponse)

        //then
        assertThat(thoughts.contains(synced)).isTrue()
    }

    @Test
    fun `unsynced thoughts are upgraded to synced upon confirmation`() {
        //given
        val uuid = "xyz"
        val unsynced = Unsynced(Text, data = "Hello World".toByteArray())
        val thoughts = ThoughtSet()
        thoughts.addThought(unsynced)

        //when
        val syncResponse = mock<SyncResponse> {
            on(mock.acknowledged) doReturn mapOf(1.toULong() to Ack(uuid))
        }

        thoughts.mergeWith(syncResponse)

        //then
        assertThat(thoughts.contains(unsynced)).isTrue()

        /* note that a test like this would fail:
           assertThat(thoughts.contains(unsynced)).isEqualTo(false))
           the reason for that is that `unsynced` and `synced` are, by design, equal
           when they have the same UUID
         */
        //so we have to manually check that the thought is the correct one:
        assertThat(thoughts.size).isEqualTo(1)
        thoughts.forEach {
            assertThat(it).isInstanceOf(Synced::class.java)
            assertThat((it as Synced).UUID).isEqualTo(uuid)
            assertThat(it.timestampCreated).isEqualTo(unsynced.timestampCreated)
            assertThat(String(it.data)).isEqualTo("Hello World")
        }
    }

    @Test
    fun `thoughts deleted locally are removed upon confirmation`() {
        //given
        val uuid = "UUID"
        val undeleted = Undeleted(uuid)
        val thoughts = ThoughtSet()
        thoughts.addThought(undeleted)

        //when
        val syncResponse = mock<SyncResponse> {
            on(mock.deleted) doReturn setOf(uuid)
        }
        thoughts.mergeWith(syncResponse)

        //then
        assertThat(thoughts).isEmpty()
    }

    @Test
    fun `thoughts deleted remotely are removed upon delete notification`() {
        //given
        val uuid = "UUID"
        val synced = mock<Synced> {
            on(mock.UUID) doReturn uuid
        }
        val thoughts = ThoughtSet()
        thoughts.addThought(synced)

        //when
        val syncResponse = mock<SyncResponse> {
            on(mock.deleted) doReturn setOf(uuid)
        }
        thoughts.mergeWith(syncResponse)

        //then
        assertThat(thoughts).isEmpty()
    }
}