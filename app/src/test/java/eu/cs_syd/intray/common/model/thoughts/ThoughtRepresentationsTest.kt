package eu.cs_syd.intray.common.model.thoughts

import eu.cs_syd.intray.Utils
import eu.cs_syd.intray.app.service.Encoder
import eu.cs_syd.intray.common.data.toJson
import eu.cs_syd.intray.common.model.thoughts.Thought.Synced
import eu.cs_syd.intray.common.model.thoughts.Thought.Unsynced
import eu.cs_syd.intray.config.DataConfig
import eu.cs_syd.intray.testutils.data.TestDataEncodings
import eu.cs_syd.intray.testutils.data.TestDateStrings
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SyncRepresentationTests {

    @BeforeEach
    fun setup() {
        Encoder.base64EncoderDecoder = Utils.javaBase64EncoderDecoder
    }

    @Test
    fun `synced sync representation consists of UUID only`() {
        val expectedSyncRepresantation = "xyz"

        val jsonRepresentation = """
            {
                "id" : "xyz",
                "created" : "${TestDateStrings.on_2020_04_10_A}",
                "contents" : {
                    "type" : "${ContentType.Text}",
                    "data" : "${TestDataEncodings.helloWorld}"
                }
            }
        """.toJson()

        val synced = Synced(jsonRepresentation)

        assertThat(synced.syncRepresentation).isEqualTo(expectedSyncRepresantation)
    }

    @Test
    fun `unsynced sync representation is correct`() {
        val unsynced = Unsynced(
            type = ContentType.Text,
            data = "Hello World".toByteArray(),
            timestampCreated = DataConfig.dateFormat.parse(TestDateStrings.on_2020_04_10_A)
        )

        assertThat(unsynced.syncRepresentation).isEqualTo("""
            {
                "contents" : {
                    "type" : "text",
                    "data" : "${TestDataEncodings.helloWorld}"
                },
                "created" : "${TestDateStrings.on_2020_04_10_A}"
            }
        """.toJson())
    }

    @Test
    fun `undeleted sync representation is correct`() {
        val uuid = "xyz"
        val undeleted = Thought.Undeleted(uuid)

        assertThat(undeleted.syncRepresentation).isEqualTo(uuid)
    }

}