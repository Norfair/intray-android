package eu.cs_syd.intray.user.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class UserEncoding {

    @Test
    fun userWithInvalidCredentials() {
        //given
        val userName = "foo"
        val serverName = "somewhere"
        val userCredentials = Credentials.Invalid
        val user = User(userName, serverName, userCredentials)

        //when
        val userEncoding = user.stringEncoding
        val reconstruction = User.fromEncoding(userEncoding)

        //then
        assertThat(reconstruction.name).isEqualTo(user.name)
        assertThat(reconstruction.serverName).isEqualTo(user.serverName)
        assertThat(reconstruction.creds).isEqualTo(user.creds)
        assertThat(reconstruction.isLoggedIn).isEqualTo(user.isLoggedIn)
        assert(!reconstruction.isLoggedIn)
    }


    @Test
    fun userWithValidCredentials() {
        //given
        val userName = "foo"
        val serverName = "somewhere"
        val userCredentials = Credentials.Valid("\$DUMMY-JWT")
        val user = User(userName, serverName, userCredentials)

        //when
        val userEncoding = user.stringEncoding
        val reconstruction = User.fromEncoding(userEncoding)

        //then
        assertThat(reconstruction.name).isEqualTo(user.name)
        assertThat(reconstruction.serverName).isEqualTo(user.serverName)
        assertThat(reconstruction.creds).isEqualTo(user.creds)
        assertThat(reconstruction.isLoggedIn).isEqualTo(user.isLoggedIn)
        assert(reconstruction.isLoggedIn)
    }
}