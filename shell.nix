{ pkgs ? import ./nix/pkgs.nix
, ...
}:
let
  nix-pre-commit-hooks =
    import (
      builtins.fetchTarball "https://github.com/hercules-ci/nix-pre-commit-hooks/archive/5c3078ad58856ce22f883b5518879d27bfc59dd5.tar.gz"
    );

  pre-commit-hook = nix-pre-commit-hooks.run {
    src = ./.;
    hooks = {
      nixpkgs-fmt.enable = true;
    };
  };

  fhs = pkgs.buildFHSUserEnv {
    name = "android-env";
    targetPkgs = pkgs: with pkgs;
      [
        androidenv.androidPkgs_9_0.platform-tools
        git
        gitRepo
        gnupg
        python2
        curl
        procps
        openssl
        gnumake
        nettools
        android-studio
        jdk
        schedtool
        utillinux
        m4
        gperf
        perl
        libxml2
        zip
        unzip
        bison
        flex
        lzop
      ];
    multiPkgs = pkgs: with pkgs;
      [
        zlib
      ];
    runScript = "bash";
    profile = ''
      export USE_CCACHE=1
      export ANDROID_JAVA_HOME=${pkgs.jdk.home}
    '';
  };
in
pkgs.mkShell rec {
  name = "nixops-shell";

  buildInputs =
    with pkgs;
    [
      android-studio
      fhs
    ];

  shellHook = ''
    ${pre-commit-hook.shellHook}

    export TERM=xterm
    exec android-env
  '';
}
