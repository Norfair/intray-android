let
  pkgsv = import (import ./nixpkgs.nix);
  cleanPkgs = pkgsv {};
  pkgs =
    pkgsv {
      overlays =
        [
          (import ./gitignore-src.nix)
          # ( import ./overlay.nix )
        ];
      config.allowUnfree = true;
    };
in
pkgs
